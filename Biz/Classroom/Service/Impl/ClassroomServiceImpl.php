<?php

namespace SubSchoolPlugin\Biz\Classroom\Service\Impl;

use Biz\Classroom\Service\Impl\ClassroomServiceImpl as ClassroomBaseServiceImpl;

class ClassroomServiceImpl extends ClassroomBaseServiceImpl
{
    protected function _prepareClassroomConditions($conditions)
    {
        $conditions = parent::_prepareClassroomConditions($conditions);

        if (!isset($conditions['likeOrgCode']) && !isset($conditions['orgCode']) && !isset($conditions['orgId'])) {
            $conditions['orgCode'] = $this->getCurrentUser()->getSelectOrgCode();
        }

        return $conditions;
    }
}
