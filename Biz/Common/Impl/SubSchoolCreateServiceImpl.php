<?php
namespace SubSchool\Service\Common\Impl;

use Topxia\Common\ArrayToolkit;
use Topxia\Service\Common\BaseService;
use Topxia\Service\Common\ServiceKernel;
use SubSchool\Service\Common\SubSchoolCreateService;

class SubSchoolCreateServiceImpl extends BaseService implements SubSchoolCreateService
{
    public function createSubSchool($fields, $type)
    {
        $connection = ServiceKernel::instance()->getConnection();
        try {
            $connection->beginTransaction();
            //创建组织机构
            $org = array(
                'name' => $fields['name'],
                'code' => $fields['site'],
                'parentId' => 1,
            );
            $org = $this->getOrgService()->createOrg($org);
            //设置setting
            $site = array(
                'name'      =>  $fields['name'],
                'slogan'    =>  '',
                'logo'      =>  $fields['logo']
            );
            $this->getSettingService()->setByNamespace('org-'.$org['id'], 'site', $site);
            if ($type == 'audit') {
                //更新分校状态
                $school = $this->getSubSchoolService()->updateSubSchool($fields['id'], array(
                    'status' => 'normal', 
                    'adminIds' => array($fields['applicantId']),
                    'orgCode' => $org['orgCode'],
                    'orgId' => $org['id'],
                    )
                );
                $this->getSubSchoolService()->createAuditRecord($fields);
                //更改用户组织机构和roles
                $user = $this->getUserService()->changeUserOrg($school['applicantId'],$org['orgCode']);
                $roles = array_merge(array("ROLE_SCHOOL_ADMIN", "ROLE_TEACHER"), $this->getUserService()->getUser($school['applicantId'])['roles']);
                $user = $this->getUserService()->changeUserRoles($school['applicantId'], array_unique($roles));
                //添加日志
                $this->getLogService()->info('sub_school', 'approval_succes_sub_school', "管理员审核通过分校({$school['name']}#{$school['id']})", $school);
                $this->getLogService()->info('system', 'update_settings', "更新站点设置", $site);
            } elseif ($type == 'create') {
                $fields['orgId'] = $org['id'];
                $fields['orgCode'] = $org['orgCode'];
                foreach ($fields['adminIds'] as $adminId) {
                    $this->getUserService()->changeUserOrg($adminId, $fields['orgCode']);
                    $roles = array_merge(array("ROLE_SCHOOL_ADMIN", "ROLE_TEACHER"), $this->getUserService()->getUser($adminId)['roles']);
                    $this->getUserService()->changeUserRoles($adminId, array_unique($roles));
                }

                $subSchoolResult = $this->getSubSchoolService()->createSubSchool($fields);
                $this->getLogService()->info('sub_school', 'create_sub_school', "管理员创建分校({$subSchoolResult['name']}#{$subSchoolResult['id']})", $subSchoolResult);
                $this->getLogService()->info('system', 'update_settings', "更新站点设置", $site);
            }

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }
    }

    protected function getSubSchoolService()
    {
        return $this->createService('SubSchool:SubSchool.SubSchoolService');
    }

    protected function getOrgService()
    {
        return $this->createService('Org:Org.OrgService');
    }

    protected function getSettingService()
    {
        return $this->createService('System.SettingService');
    }

    protected function getUserService()
    {
        return $this->createService('User.UserService');
    }

    protected function getLogService()
    {
        return $this->createService('System.LogService');
    }
}