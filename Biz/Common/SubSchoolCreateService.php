<?php
namespace SubSchool\Service\Common;

interface SubSchoolCreateService
{
    public function createSubSchool($fields, $type);
}