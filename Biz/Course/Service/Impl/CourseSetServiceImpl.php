<?php

namespace SubSchoolPlugin\Biz\Course\Service\Impl;

use Biz\Course\Service\Impl\CourseSetServiceImpl as CourseSetBaseServiceImpl;
use Biz\AppLoggerConstant;

class CourseSetServiceImpl extends CourseSetBaseServiceImpl
{
    protected function prepareConditions($conditions)
    {
        $conditions = parent::prepareConditions($conditions);

        if (!isset($conditions['likeOrgCode']) && !isset($conditions['orgCode']) && !isset($conditions['orgId'])) {
            $conditions['orgCode'] = $this->getCurrentUser()->getSelectOrgCode();
        }

        if (isset($conditions['likeOrgCode']) && !empty($conditions['likeOrgCode'])) {
            $conditions['orgCode'] = $conditions['likeOrgCode'];
        }

        if (isset($conditions['likeAllOrgCode']) && !empty($conditions['likeAllOrgCode'])) {
            unset($conditions['orgCode']);
            $conditions['likeOrgCode'] = $conditions['likeAllOrgCode'];
        }

        return $conditions;
    }

    public function copyCourseSetToOrgCode($courseSetId, $orgCode)
    {
        $courseSet = $this->getCourseSetDao()->get($courseSetId);
        $org = $this->getOrgService()->getOrgByOrgCode($orgCode);

        try {
            $this->beginTransaction();
            $courseSet = $this->getCourseSet($courseSetId);

            if (empty($courseSet)) {
                $this->createNotFoundException('courseSet not found');
            }
            if (empty($org)) {
                $this->createNotFoundException('org not found');
            }
            if ($courseSet['orgId'] == $org['id']) {
                $this->createInvalidArgumentException('can not copy course ');
            }
            
            $courseSet['orgId'] = $org['id'];
            $courseSet['orgCode'] = $org['orgCode'];

            $this->biz['course_set_courses_copy']->copy($courseSet, array('params' => array()));
            $conditions = array(
                'title' => $courseSet['title'],
                'parentId' => 0,
                'orgId' => $org['id'],
                'orgCode' => $org['orgCode']
            );
            $copyCourses = $this->searchCourseSets(
                $conditions,
                array('createdTime' => 'DESC'),
                0,
                1
            );

            if (!empty($copyCourses) && $org['id'] != 1) {
                $this->changeCourseSetTeachers($copyCourses[0]['id'], $org['id']);
            }

            $this->getLogService()->info(AppLoggerConstant::COURSE, 'clone_course_set', "推送课程 - {$courseSet['title']}(#{$courseSetId}) 成功", array('courseSetId' => $courseSetId));
            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();
            $this->getLogService()->error(AppLoggerConstant::COURSE, 'clone_course_set', "推送课程 - {$courseSet['title']}(#{$courseSetId}) 失败");

            throw $e;
        }

        return $courseSet;
    }

    public function changeCourseSetOrgIdAndOrgCod($courseSetId, $fields)
    {
        $courseSet = $this->getCourseDao()->getCourse($courseSetId);

        if (empty($courseSet)) {
            throw $this->createServiceException('课程不存在，更新失败！');
        }

        $fields = ArrayToolkit::parts($fields, array('orgId', 'orgCode'));
        $this->getCourseDao()->updateCourse($courseSetId, $fields);
        $this->getLogService()->info('course', 'update', "更新课程《{$course['title']}》(#{$course['id']})的信息", $fields);
    }

    public function changeCourseSetTeachers($courseSetId, $orgId)
    {
        $subschool = $this->getSubSchoolService()->getSubSchoolByOrgId($orgId);
        $courseSet = $this->getCourseSetService()->getCourseSet($courseSetId);
        if (empty($courseSet)) {
            throw $this->createServiceException('课程不存在，更新失败！');
        }

        $fields = array(
            'teacherIds' => $subschool['adminIds']
        );

        try {
            $this->beginTransaction();
            $this->getCourseSetService()->updateCourseSetTeacherIds($courseSetId, $subschool['adminIds']);
            $this->getCourseMemberService()->deleteMemberByCourseIdAndRole($courseSetId, 'teacher');

            $teachers = array();
            foreach ($subschool['adminIds'] as $index => $teacherId) {
                $teachers[$index]['id'] = $teacherId;
                $teachers[$index]['isVisible'] = 1;
            }
            $this->getCourseMemberService()->setCourseTeachers($courseSetId, $teachers);
            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();
            $this->getLogService()->error(AppLoggerConstant::COURSE, 'clone_course_set', "推送课程 - {$courseSet['title']}(#{$courseSetId}) 失败");

            throw $e;
        }
    }

    /**
     * @return \Biz\Org\Service\OrgService
     */
    protected function getOrgService()
    {
        return $this->createService('Org:OrgService');
    }

    /**
     * @return \Biz\Org\Service\OrgService
     */
    protected function getSubSchoolService()
    {
        return $this->createService('SubSchoolPlugin:SubSchool:SubSchoolService');
    }

    protected function getCourseSetService()
    {
        return $this->createService('Course:CourseSetService');
    }

    protected function getCourseMemberService()
    {
        return $this->createService('Course:MemberService');
    }
}
