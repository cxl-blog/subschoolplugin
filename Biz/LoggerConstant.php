<?php

namespace SubSchoolPlugin\Biz;

use Biz\LoggerConstantInterface;

class LoggerConstant implements LoggerConstantInterface
{
    /**
     * [$sub_school 分校].
     *
     * @var string
     */
    const SubSchool = 'sub_school';

    public function getActions()
    {
        return array(
            self::SubSchool => array(
                'create_sub_school',
                'update_sub_school'
            ),
        );
    }

    public function getModules()
    {
        return array(
            self::SubSchool,
        );
    }
}