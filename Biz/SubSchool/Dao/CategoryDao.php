<?php

namespace SubSchoolPlugin\Biz\SubSchool\Dao;

use Codeages\Biz\Framework\Dao\GeneralDaoInterface;

interface CategoryDao extends GeneralDaoInterface
{
    public function findAll();

    public function findByIds(array $ids);

    // public function createCategory($category);

    public function getCategoriesByParentId($id);

    // public function getCategory($id);

    // public function updateCategory($id, $fields);

    // public function deleteCategory($id);

    // public function searchCategoryCount($conditions);

    // public function searchCategories($conditions, $orderBys, $start, $limit);

    public function getCategoryByCode($code);

    public function findRootCategories();

    public function getCategoryByName($name);
}
