<?php

namespace SubSchoolPlugin\Biz\SubSchool\Dao\Impl;

use SubSchoolPlugin\Biz\SubSchool\Dao\CategoryDao;
use Codeages\Biz\Framework\Dao\GeneralDaoImpl;

class CategoryDaoImpl extends GeneralDaoImpl implements CategoryDao
{
    protected $table = 'sub_school_category';

    public function declares()
    {
        return array(
            'serializes' => array(),
            'orderbys'   => array('createdTime', 'seq'),
            'timestamps' => array('createdTime', 'updatedTime'),
            'conditions' => array(
                'name = :name',
                'code = :code',
                'parentId = :parentId'
            )
        );
    }

    public function findByIds(array $ids)
    {
        if (empty($ids)) {
            return array();
        }
        $marks = str_repeat('?,', count($ids) - 1).'?';

        $keys = implode(',', $ids);

        $sql = "SELECT * FROM {$this->table()} WHERE id IN ({$marks});";

        return $this->db()->fetchAll($sql, $ids);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM {$this->table()} ORDER BY `seq` ASC ;";

        return $this->db()->fetchAll($sql);
    }

    // public function createCategory($category)
    // {
    //     $category['createdTime'] = time();
    //     $category['updatedTime'] = time();
    //     $affected = $this->getConnection()->insert($this->getTable(), $category);

    //     if ($affected <= 0) {
    //         throw $this->createDaoException("Insert {$this->getTable()} error.");
    //     }

    //     return $this->getCategory($this->getConnection()->lastInsertId());
    // }

    public function getCategoriesByParentId($id)
    {
        return $this->findByFields(array(
            'parentId' => $id
        ));
    }

    // public function updateCategory($id, $fields)
    // {
    //     $fields['updatedTime'] = time();
    //     $this->getConnection()->update($this->getTable(), $fields, array('id' => $id));
    //     return $this->getCategory($id);
    // }

    // public function deleteCategory($id)
    // {
    //     return $this->getConnection()->delete($this->getTable(), array('id' => $id));
    // }

    // public function searchCategoryCount($conditions)
    // {
    //     $builder = $this->_createSearchQueryBuilder($conditions)
    //         ->select('COUNT(*)') ;

    //     return $builder->execute()->fetchColumn(0);
    // }

    // public function searchCategories($conditions, $orderBys, $start, $limit)
    // {
    //     $this->filterStartLimit($start, $limit);
    //     $builder = $this->_createSearchQueryBuilder($conditions)
    //         ->select('*')
    //         ->setFirstResult($start)
    //         ->setMaxResults($limit);

    //     foreach ($orderBys as $orderBy) {
    //         $builder->addOrderBy($orderBy[0], $orderBy[1]);
    //     }

    //     return $builder->execute()->fetchAll() ?: array();
    // }

    // public function getCategory($id)
    // {
    //     $sql = "SELECT * FROM {$this->getTable()} WHERE id = ? LIMIT 1";

    //     return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    // }

    public function getCategoryByCode($code)
    {
        return $this->getByFields(array(
            'code' => $code
        ));
    }

    public function findRootCategories()
    {
        $sql = "SELECT * FROM {$this->table} WHERE parentId = ?;";

        return $this->db()->fetchAll($sql, array(0));
    }

    // protected function _createSearchQueryBuilder($conditions)
    // {
    //     $builder = $this->createDynamicQueryBuilder($conditions)
    //         ->from($this->getTable())
    //         ->andWhere('code = :code')
    //         ->andWhere('name = :name')
    //         ->andWhere('parentId = :parentId')
    //         ->andWhere('id IN ( :categoryIds )')
    //     ;

    //     return $builder;
    // }

    public function getCategoryByName($name)
    {
        return $this->getByFields(array(
            'name' => $name
        ));
    }
}
