<?php
namespace SubSchool\Service\SubSchool\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use SubSchool\Service\SubSchool\Dao\SubSchoolAuditDao;

class SubSchoolAuditDaoImpl extends BaseDao implements SubSchoolAuditDao
{
    protected $table = 'sub_school_audit_record';

    public function createAuditRecord($field)
    {
        $affected = $this->getConnection()->insert($this->table , $field);

        if ($affected <= 0) {
            throw $this->createDaoException('Insert sub_school error.');
        }

        return $this->getAuditRecord($this->getConnection()->lastInsertId());
    }

    public function getAuditRecord($id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE id = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    public function searchAuditRecordCount($conditions)
    {
        $builder = $this->_createSearchQueryBuilder($conditions)->select('COUNT(id)');

        return $builder->execute()->fetchColumn(0);
    }

    public function searchAuditRecords($conditions, $sort, $start, $limit)
    {
        $this->filterStartLimit($start, $limit);
        $builder = $this->_createSearchQueryBuilder($conditions)
                        ->select('*')
                        ->orderBy($sort[0],$sort[1])
                        ->setFirstResult($start)
                        ->setMaxResults($limit);

        return $builder->execute()->fetchAll() ? :array();
    }

    protected function _createSearchQueryBuilder($conditions)
    {
        if (isset($conditions['name'])) {
            $conditions['name'] = '%' . $conditions['name'] . '%';
        }

        $builder = $this->createDynamicQueryBuilder($conditions)
            ->from($this->table, $this->table)
            ->andWhere('schoolId = :schoolId')
            ->andWhere('schoolIds IN (:schoolIds)')
            ->andWhere('name LIKE :name');

        return $builder;
    }
}