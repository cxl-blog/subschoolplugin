<?php

namespace SubSchoolPlugin\Biz\SubSchool\Dao\Impl;

use SubSchoolPlugin\Biz\SubSchool\Dao\SubSchoolDao;
use Codeages\Biz\Framework\Dao\GeneralDaoImpl;

class SubSchoolDaoImpl extends GeneralDaoImpl implements SubSchoolDao
{
    protected $table = 'sub_school';

    public function declares()
    {
        return array(
            'serializes' => array(
                'adminIds' => 'delimiter',
            ),
            'orderbys'   => array('createdTime', 'recommendedSeq'),
            'timestamps' => array('createdTime', 'updatedTime'),
            'conditions' => array(
                'name = :name',
                'recommended = :recommended',
                'id IN (　:ids)',
                'categoryId = :categoryId',
                'categoryId IN (:categoryIds)',
                'province = :province',
                'provinces IN ( :provinces)',
                'status = :status'
            )
        );
    }

    public function getByName($name)
    {
        return $this->getByFields(array('name' => $name));
    }

    public function getBySite($site)
    {
        return $this->getByFields(array('site' => $site));
    }

    public function getByOrgId($orgId)
    {
        // TODO: Implement getByOrgId() method.
        return $this->getByFields(array('orgId' => $orgId));
    }

    //            ->andWhere('id = :id')
//            ->andWhere('id IN (　:ids)')
//            ->andWhere('name  LIKE :nameLike')
//            ->andWhere('subSchoolCategoryId = :subSchoolCategoryId')
//            ->andWhere('subSchoolCategoryId IN (:categoryIds)')
//            ->andWhere('recommended =:recommended')
//            ->andWhere('province = :province')
//            ->andWhere('provinces IN ( :provinces)')
//            ->andWhere('status = :status')
//            ->andWhere('status IN ( :allStatus)');

//    public function getSubSchool($id)
//    {
//        $sql = "SELECT * FROM {$this->table} WHERE id = ? LIMIT 1";
//        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
//    }
//
//    public function addSubSchool($subSchool)
//    {
//        $subSchool['createdTime'] = time();
//        $affected = $this->getConnection()->insert($this->table, $subSchool);
//
//        if ($affected <= 0) {
//            throw $this->createDaoException('Insert sub_school error.');
//        }
//
//        return $this->getSubSchool($this->getConnection()->lastInsertId());
//    }
//
//    public function updateSubSchool($id, $fields)
//    {
//        $this->getConnection()->update($this->table, $fields, array('id' => $id));
//
//        return $this->getSubSchool($id);
//    }
//
//    public function findSubSchoolByOrgId($orgId)
//    {
//        $sql = "SELECT * FROM {$this->getTable()} WHERE orgId = ? LIMIT 1";
//        return $this->getConnection()->fetchAssoc($sql, array($orgId));
//    }
//
//    public function getSubSchoolBySchoolSite($site)
//    {
//        $sql = "SELECT * FROM {$this->getTable()} WHERE site = ? LIMIT 1";
//        return $this->getConnection()->fetchAssoc($sql, array($site));
//    }
//
//    public function searchSubSchools($conditions, $orderBys, $start, $limit)
//    {
//        $this->filterStartLimit($start, $limit);
//        $builder = $this->_createSearchQueryBuilder($conditions)
//            ->select('*')
//            ->setFirstResult($start)
//            ->setMaxResults($limit);
//
//        for ($i = 0; $i < count($orderBys); $i = $i + 2) {
//            $builder->addOrderBy($orderBys[$i], $orderBys[$i + 1]);
//        };
//
//        return $builder->execute()->fetchAll() ?: array();
//    }
//
//    public function searchSubSchoolsCount($conditions)
//    {
//        $builder = $this->_createSearchQueryBuilder($conditions)
//            ->select('COUNT(id)');
//        return $builder->execute()->fetchColumn(0);
//    }
//
//    public function findSubSchoolsByLikeName($name)
//    {
//        $sql = "SELECT * FROM {$this->table} WHERE name LIKE '%$name%'";
//        return $this->getConnection()->fetchAll($sql, array($name)) ?: null;
//    }
//
//    protected function _createSearchQueryBuilder($conditions)
//    {
//        if (isset($conditions['name'])) {
//            $conditions['nameLike'] = "%{$conditions['name']}%";
//            unset($conditions['name']);
//        }
//
//        $builder = $this->createDynamicQueryBuilder($conditions)
//            ->from($this->table, 'sub_school')
//            ->andWhere('id = :id')
//            ->andWhere('id IN (　:ids)')
//            ->andWhere('name  LIKE :nameLike')
//            ->andWhere('subSchoolCategoryId = :subSchoolCategoryId')
//            ->andWhere('subSchoolCategoryId IN (:categoryIds)')
//            ->andWhere('recommended =:recommended')
//            ->andWhere('province = :province')
//            ->andWhere('provinces IN ( :provinces)')
//            ->andWhere('status = :status')
//            ->andWhere('status IN ( :allStatus)');
//        return $builder;
//    }
//
//    public function getSubSchoolByName($name)
//    {
//        $sql = "SELECT * FROM {$this->getTable()} WHERE name = ? LIMIT 1";
//        return $this->getConnection()->fetchAssoc($sql, array($name)) ?: null;
//    }
//
//    public function getSubSchoolByApplicantIdAndStatus($applicantId, $status)
//    {
//        $sql = "SELECT * FROM {$this->table} WHERE status = ? and applicantId = ? LIMIT 1";
//        return $this->getConnection()->fetchAssoc($sql, array($status, $applicantId)) ?: null;
//    }
//
//    public function deleteSubSchool($id)
//    {
//        $affected = $this->getConnection()->delete($this->table, array('id' => $id));
//
//        if ($affected <= 0) {
//            throw $this->createDaoException('delete branchSchool error. ');
//        }
//
//        return $this->getSubSchool($this->getConnection()->lastInsertId());
//    }

    public function findSubSchoolBySubSchoolCategoryId($fields)
    {
        return $this->findByFields($fields);
    }

    public function findByStatus($status)
    {
        return $this->findByFields(array('status' => $status));
    }
}
