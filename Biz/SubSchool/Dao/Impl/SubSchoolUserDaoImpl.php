<?php

namespace SubSchool\Service\SubSchool\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use SubSchool\Service\SubSchool\Dao\SubSchoolUserDao;

class SubSchoolUserDaoImpl extends BaseDao implements SubSchoolUserDao
{
    protected $table = 'sub_school_user';

    public function createSchoolUser($user)
    {
        $affected = $this->getConnection()->insert($this->table, $user);

        if ($affected <= 0) {
            throw $this->createDaoException('Insert schoolUser error. ');
        }

        return $this->getSchoolUser($this->getConnection()->lastInsertId());
    }

    public function getSchoolUser($id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE id = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    public function getUserByUserIdAndSchoolId($userId, $schoolId)
    {
        $sql = "SELECT * FROM {$this->table} WHERE userId = ? AND schoolId = ?;";

        return $this->getConnection()->fetchAssoc($sql, array($userId, $schoolId)) ?:null;
    }

    public function findSchoolUsersByUserId($id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE userId = ?;";

        return $this->getConnection()->fetchAll($sql, array($id)) ?: null;
    }

    public function findUserIdsBySchoolId($schoolId)
    {
        $sql = "SELECT userId FROM {$this->table} WHERE schoolId = ?;";

        return $this->getConnection()->fetchAll($sql, array($schoolId)) ?: null;
    }

    public function deleteUserByUserId($userId)
    {
        $affected = $this->getConnection()->delete($this->table, array('userId' => $userId
            ));

        if ($affected <= 0) {
            throw $this->createDaoException('delete branchSchool error. ');
        }

        return $this->getSchoolUser($this->getConnection()->lastInsertId());
    }
}