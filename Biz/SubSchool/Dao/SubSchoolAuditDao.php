<?php
namespace SubSchool\Service\SubSchool\Dao;

interface SubSchoolAuditDao
{
    public function createAuditRecord($field);

    public function getAuditRecord($id);

    public function searchAuditRecordCount($conditions);

    public function searchAuditRecords($conditions, $sort, $start, $limit);
}
