<?php

namespace SubSchoolPlugin\Biz\SubSchool\Dao;

use Codeages\Biz\Framework\Dao\GeneralDaoInterface;

interface SubSchoolDao extends GeneralDaoInterface
{
    public function getByName($name);

    public function getBySite($site);

//  public function addSubSchool($subSchool);
//
//  public function updateSubSchool($id,$subSchool);
//
//  public function searchSubSchools($conditions,$orderBy ,$start ,$limit);
//
////    public function searchSubSchoolsCount($conditions);
//
//  public function findSubSchoolByOrgId($orgId);
//
//    public function findSubSchoolsByLikeName($name);
//
//    public function getSubSchoolByApplicantIdAndStatus($applicantId, $status);
//
//    public function deleteSubSchool($id);

    public function findSubSchoolBySubSchoolCategoryId($fields);

    public function getByOrgId($orgId);

    public function findByStatus($status);

}
