<?php

namespace SubSchool\Service\SubSchool\Dao;

interface SubSchoolUserDao
{
    public function createSchoolUser($user);

    public function getSchoolUser($id);

    public function findSchoolUsersByUserId($id);

    public function getUserByUserIdAndSchoolId($userId, $schoolId);

    public function findUserIdsBySchoolId($schoolId);

    public function deleteUserByUserId($userId);
}