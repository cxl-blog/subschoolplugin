<?php

namespace SubSchoolPlugin\Biz\SubSchool\Service;

interface CategoryService
{
    public function createCategory($category);

    public function getCategoryTree();

    public function getCategory($id);

    public function getCategoriesByParentId($id);

    public function updateCategory($id, $fields);

    public function deleteCategory($id);

    public function searchCategories($conditions, $orderBy, $start, $limit);

    public function findCategoriesByIds(array $ids);

    public function sortCategories($ids);

    public function getCategoryByCode($code);

    public function findCategoryChildrenIds($id);

    public function makeNavCategories($code);

    public function findCategoryByName($name);
}
