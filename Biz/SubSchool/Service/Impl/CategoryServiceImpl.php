<?php

namespace SubSchoolPlugin\Biz\SubSchool\Service\Impl;

use AppBundle\Common\TreeToolkit;
use AppBundle\Common\ArrayToolkit;
use Biz\BaseService;
use SubSchoolPlugin\Biz\SubSchool\Service\CategoryService;

class CategoryServiceImpl  extends BaseService implements CategoryService
{
    public function sortCategories($ids){

        foreach ($ids as $index => $id) {
            $this->getCategoryDao()->update($id, array('seq' => $index + 1));
        }

    }

    public function getCategory($id)
    {
        if (empty($id)) {
            return array();
        }
        return $this->getCategoryDao()->get($id);
    }

    public function getCategoriesByParentId($id)
    {
        return $this->getCategoryDao()->getCategoriesByParentId($id);
    }

    public function createCategory($category)
    {
        $category = ArrayToolkit::parts($category,
            array(
                'name',
                'code',
                'parentId',
                'seq'
                ));
        if (!ArrayToolkit::requireds($category, array('name', 'code'))) {
            throw $this->createServiceException("缺少必要参数，，添加栏目失败");
        }

        $this->_filterCategoryFields($category);

        $category = $this->getCategoryDao()->create($category);
        $this->getLogService()->info('sub_school', 'create_category', "添加分校分类,{$category['name']}(#{$category['id']})");

        return $category;
    }

    public function getCategoryTree()
    {
        $categories = $this->prepareTree($this->findAllCategories());
        $tree       = array();
        $this->makeCategoryTree($tree, $categories, 0);

        return $tree;
    }

    protected function prepareTree($categories)
    {
        $prepared = array();

        foreach ($categories as $category) {
            if (!isset($prepared[$category['parentId']])) {
                $prepared[$category['parentId']] = array();
            }

            $prepared[$category['parentId']][] = $category;
        }

        return $prepared;    
    }

    protected function makeCategoryTree(&$tree, &$categories, $parentId)
    {
        static $depth = 0;
        static $leaf  = false;

        if (isset($categories[$parentId]) && is_array($categories[$parentId])) {
            foreach ($categories[$parentId] as $category) {
                ++$depth;
                $category['depth'] = $depth;
                $tree[] = $category;
                $this->makeCategoryTree($tree, $categories, $category['id']);
                --$depth;
            }
        }

        return $tree;
    }

    public function makeNavCategories($code)
    {
        $rootCagoies = $this->getCategoryDao()->findRootCategories();

        if (empty($code)) {
            return array($rootCagoies, array(), array());
        } else {
            $category    = $this->getCategoryByCode($code);
            $parentId    = $category['id'];
            $categories  = array();
            $activeIds   = array();
            $activeIds[] = $category['id'];
            $level       = 1;

            while ($parentId) {
                $activeIds[] = $parentId;
                $sibling     = $this->getCategoriesByParentId($parentId);

                if ($sibling) {
                    $categories[$level] = $sibling;
                    $level++;
                }

                $parent   = $this->getCategory($parentId);
                $parentId = $parent['parentId'];
            }

            $categories = array_reverse($categories);
            return array($rootCagoies, $categories, $activeIds);
        }
    }

    public function updateCategory($id, $fields)
    {
        $category = $this->getCategory($id);
        if (empty($category)) {
            throw $this->createNoteFoundException("栏目(#{$id})不存在，更新栏目失败！");
        }

        $fields = ArrayToolkit::parts($fields, array('name', 'code', 'parentId', 'seq'));
        if (empty($fields)) {
            throw $this->createServiceException('参数不正确，更新栏目失败！');
        }

        $this->_filterCategoryFields($fields);

        $status  = $this->getCategoryDao()->update($id, $fields);
        $this->getLogService()->info('sub_school', 'update_category', "编辑栏目 {$fields['name']}(#{$id})", $fields);

        return  $status;
    }

    public function deleteCategory($id)
    {
        $category = $this->getCategory($id);
        if (empty($category)) {
            throw $this->createNotFoundException();
        }

        if($category['name'] == 'default'){
            throw $this->createNoteFoundException("分校默认分类(#{$category['name']})不能删除！");
        }
        $this->getCategoryDao()->delete($category['id']);

        $this->getLogService()->info('sub_school', 'delete_category', "删除分校分类,{$category['name']}(#{$category['id']})");
    }

    public function findCategoriesByParentId($parentId)
    {
        $conditions = array('parentId' => $parentId);
        $count = $this->getCategoryDao()->count($conditions);
        $orderBys = array(
            'seq' => 'ASC');

        return $this->getCategoryDao()->search($conditions, $orderBys, 0, $count);
    }

    public function searchCategories($conditions, $orderBy, $start, $limit)
    {
        return $this->getCategoryDao()->search($conditions, $orderBy, $start, $limit);
    }

    public function getCategoryByCode($code)
    {
        return $this->getCategoryDao()->getCategoryByCode($code);
    }

    public function findCategoryChildrenIds($id)
    {
        $category = $this->getCategory($id);

        if (empty($category)) {
            return array();
        }

        $tree = $this->getCategoryTree();

        $childrenIds = array();
        $depth       = 0;

        foreach ($tree as $node) {
            if ($node['id'] == $category['id']) {
                $depth = $node['depth'];
                continue;
            }

            if ($depth > 0 && $depth < $node['depth']) {
                $childrenIds[] = $node['id'];
            }

            if ($depth > 0 && $depth >= $node['depth']) {
                break;
            }
        }

        return $childrenIds;
    }

    protected function _filterCategoryFields($fields)
    {
        $fields = ArrayToolkit::filter($fields, array(
            'name'           => '',
            'code'           => '',
            'parentId'       => 0,
            'seq'       => 0,
        ));

        if (empty($fields['name'])) {
            throw $this->createServiceException("名称不能为空，保存栏目失败");
        }

        if (empty($fields['code'])) {
            throw $this->createServiceException("编码不能为空，保存栏目失败");
        } else {
            if (!preg_match("/^[a-zA-Z0-9_]+$/i", $fields['code'])) {
                throw $this->createServiceException("编码({$fields['code']})含有非法字符，保存栏目失败");
            }

            if (ctype_digit($fields['code'])) {
                throw $this->createServiceException("编码({$fields['code']})不能全为数字，保存栏目失败");
            }
        }

        return $fields;
    }

    public function findCategoryByName($name)
    {
        return $this->getCategoryDao()->getCategoryByName($name);
    }

    public function findAllCategories()
    {
        $categories = $this->getCategoryDao()->findAll();

        return ArrayToolkit::index($categories, 'id');
    }

    public function findCategoriesByIds(array $ids)
    {
        $categories = $this->getCategoryDao()->findByIds($ids);
        if (!empty($categories)) {
            ArrayToolkit::index($categories, 'id');
        }

        return $categories;
    }

    /**
     * @return SubSchoolCategoryDaoImpl
     */
    protected function getCategoryDao()
    {
        return $this->createDao('SubSchoolPlugin:SubSchool:CategoryDao');
    }

    protected function getLogService()
    {
        return $this->createService('System:LogService');
    }
}
