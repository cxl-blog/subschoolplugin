<?php

namespace SubSchoolPlugin\Biz\SubSchool\Service\Impl;

use AppBundle\Common\ArrayToolkit;
use Biz\BaseService;
use SubSchoolPlugin\Biz\SubSchool\Service\SubSchoolService;

class SubSchoolServiceImpl extends BaseService implements SubSchoolService
{
    public function countSubSchools($conditions)
    {
        // TODO: Implement countSubSchools() method.
        return $this->getSubSchoolDao()->count($conditions);
    }

    public function searchSubSchools($conditions, array $orderBy, $start, $limit)
    {
        return $this->getSubSchoolDao()->search($conditions, $orderBy, $start, $limit);
    }

    public function createSubSchool($fields)
    {
        try {
            $this->beginTransaction();

            $org = $this->createOrg($fields);

            $fields['orgId'] = $org['id'];
            $fields['orgCode'] = $org['orgCode'];

            $this->setSetting($fields);

            $this->createSchool($fields);
            $userFields = array(
                'orgCode' => $fields['orgCode'],
                'addRoles' => array('ROLE_SCHOOL_ADMIN', 'ROLE_TEACHER')
            );
            $this->updateUsers($fields['adminIds'], $userFields);

            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }
    }

    protected function createOrg($fields)
    {
        $org = array(
            'name' => $fields['name'],
            'code' => $fields['site'],
            'parentId' => 1,
        );

        return $this->getOrgService()->createOrg($org);
    }

    protected function setSetting($fields)
    {
        $site = array(
            'name'   =>  $fields['name'],
            'slogan' =>  '',
            'logo'   =>  '',
        );

        $site = $this->getSettingService()->setByNamespace('org-'.$fields['orgId'], 'site', $site);

        $this->getLogService()->info('system', 'update_settings', "更新站点设置", $site);

        return $site;
    }

    protected function createSchool($fields)
    {
        if (!ArrayToolkit::requireds($fields, array('name', 'status', 'adminIds', 'site', 'cover', 'categoryId', 'orgCode', 'orgId'))) {
            throw $this->createInvalidArgumentException('缺少必要字段，创建分校失败！');
        }

        $fields = ArrayToolkit::parts($fields, array('name', 'status', 'adminIds', 'site', 'cover', 'categoryId', 'director', 'directorPhone', 'license', 'orgCode', 'orgId'
        ));

        $subSchool = $this->getSubSchoolDao()->create($fields);

        $this->getLogService()->info('sub_school', 'create', "管理员创建分校({$subSchool['name']}#{$subSchool['id']})", $subSchool);

        return $subSchool;
    }

    protected function updateUsers($ids, $fields)
    {
        foreach ($ids as $userId) {
            if (!empty($fields['orgCode'])) {
                $this->getUserService()->changeUserOrg($userId, $fields['orgCode']);
            }

            $user = $this->getUserService()->getUser($userId);
            if (!empty($fields['addRoles'])) {
                $roles = array_merge($fields['addRoles'], $user['roles']);
                $this->getUserService()->changeUserRoles($userId, array_unique($roles));
            }
            if (!empty($fields['removeRoles'])) {
                $roles = array_merge(array_diff($user['roles'], $fields['removeRoles']));
                $this->getUserService()->changeUserRoles($userId, array_unique($roles));
            }
        }
    }

    public function updateSubSchool($id, $fields)
    {
        $subSchool = $this->getSubSchool($id);
        if (empty($subSchool)) {
            throw $this->createNotFoundException('分校不存在，更新失败');
        }

        try {
            $this->beginTransaction();

            $this->updateOrg($subSchool['orgId'], $fields);
            $this->updateSetting('org-' . $subSchool['orgId'], $fields);

            $originAdminIds = $subSchool['adminIds'];
            $newAdminIds = $fields['adminIds'];
            $toRemoveAdminIds = array_diff($originAdminIds, $newAdminIds);
            $toAddAdminIds = array_diff($newAdminIds, $originAdminIds);
            $this->updateUsers($toAddAdminIds, array(
                'addRoles' => array('ROLE_SCHOOL_ADMIN', 'ROLE_TEACHER'),
                'orgCode' => $subSchool['orgCode']
            ));
            $this->updateUsers($toRemoveAdminIds, array('removeRoles' => array('ROLE_SCHOOL_ADMIN')));

            $this->updateSchool($subSchool['id'], $fields);

            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }
    }

    protected function updateSchool($id, $fields)
    {
        if (!ArrayToolkit::requireds($fields, array('name', 'status', 'adminIds', 'site', 'cover', 'categoryId'))) {
            throw $this->createInvalidArgumentException('缺少必要字段，更新分校失败！');
        }

        $fields = ArrayToolkit::parts($fields, array('name', 'status', 'adminIds', 'site', 'cover', 'categoryId', 'director', 'directorPhone', 'license', 'orgCode', 'orgId'
        ));

        $subSchool = $this->getSubSchoolDao()->update($id, $fields);

        return $subSchool;
    }

    protected function updateOrg($id, $fields)
    {
        $org = array(
            'name' => $fields['name'],
            'code' => $fields['site'],
        );

        return $this->getOrgService()->updateOrg($id, $org);
    }

    protected function updateSetting($namespace, $fields)
    {
        $site = array(
            'name' => $fields['name']
        );

        $site = $this->getSettingService()->setByNamespace($namespace, 'site', $site);
        $this->getLogService()->info('system', 'update_settings', "更新站点设置(更改站点名为{$site['name']})", $site);

        return $site;
    }

    public function isNameAvailable($name, $exclude)
    {
        $subSchool = $this->getSubSchoolDao()->getByName($name);
        if (empty($subSchool)) {
            return true;
        }

        return $subSchool['name'] === $exclude;
    }

    public function isSiteAvailable($site, $exclude)
    {
        $subSchool =  $this->getSubSchoolDao()->getBySite($site);
        if (empty($subSchool)) {
            return true;
        }

        return $subSchool['site'] === $exclude;
    }

    public function getSubSchool($id)
    {
        return $this->getSubSchoolDao()->get($id);
    }
    public function closeSubSchool($id)
    {
        $this->tryManageSubSchool($id);

        $subSchool = $this->getSubSchoolDao()->update($id, array('status' => 'disable'));

        $this->getLogService()->info('sub_school', 'close', "管理员关闭分校({$subSchool['name']}#{$subSchool['id']})", $subSchool);

        return $subSchool;
    }

    public function openSubSchool($id)
    {
        $this->tryManageSubSchool($id);

        $subSchool = $this->getSubSchoolDao()->update($id, array('status' => 'normal'));

        $this->getLogService()->info('sub_school', 'open', "管理员开启分校({$subSchool['name']}#{$subSchool['id']})", $subSchool);

        return $subSchool;
    }

    public function recommendSubSchool($id, $number)
    {
        $this->tryManageSubSchool($id);

        if (!is_numeric($number)) {
            throw $this->createAccessDeniedException('recommend seq must be number!');
        }

        $fields = array(
            'recommended' => 1,
            'recommendedSeq' => (int) $number,
            'recommendedTime' => time(),
        );

        $subSchool = $this->getSubSchoolDao()->update($id, $fields);

        $this->getLogService()->info('sub_school', 'recommend', "推荐分校《{$subSchool['name']}》(#{$subSchool['id']}),序号为{$number}");

        return $subSchool;
    }

    public function cancelRecommendSubSchool($id)
    {
        $this->tryManageSubSchool($id);

        $fields = array(
            'recommended' => 0,
            'recommendedTime' => 0,
            'recommendedSeq' => 0,
        );

        $subSchool = $this->getSubSchoolDao()->update($id, $fields);

        $this->getLogService()->info('sub_school', 'cancel_recommend', "取消推荐分校《{$subSchool['name']}》(#{$subSchool['id']})");

        return $subSchool;
    }

    public function tryManageSubSchool($id)
    {
        if (!$this->canManageSubSchool($id)) {
            throw $this->createAccessDeniedException('您无权操作！');
        }
    }

    public function findSubSchoolBySubSchoolCategoryId($categoryId)
    {
        $fields = array(
            'categoryId' => $categoryId
            );
        return $this->getSubSchoolDao()->findSubSchoolBySubSchoolCategoryId($fields);
    }

    public function getSubSchoolBySite($site)
    {
        // TODO: Implement getSubSchoolByName() method.
        return $this->getSubSchoolDao()->getBySite($site);
    }

    public function getSubSchoolByOrgId($orgId)
    {
        // TODO: Implement getSubSchoolByOrgId() method.
        return $this->getSubSchoolDao()->getByOrgId($orgId);
    }

    protected function canManageSubSchool($id)
    {
        $subSchool = $this->getSubSchool($id);
        if (empty($subSchool)) {
            throw $this->createNotFoundException("subschool{$id} not found");
        }

        $user = $this->getCurrentUser();
        if (!$user->isLogin()) {
            throw $this->createAccessDeniedException('user not login');
        }

        if (!$user->isAdmin()) {
            return false;
        }

        return true;
    }

    public function findSubSchoolsByStatus($status)
    {
        return $this->getSubSchoolDao()->findByStatus($status);
    }

    private function getSubSchoolDao()
    {
        return $this->createDao('SubSchoolPlugin:SubSchool:SubSchoolDao');
    }

    protected function getOrgService()
    {
        return $this->createService('Org:OrgService');
    }

    protected function getUserService()
    {
        return $this->createService('User:UserService');
    }

    protected function getSettingService()
    {
        return $this->createService('System:SettingService');
    }

    protected function getLogService()
    {
        return $this->createService('System:LogService');
    }
}
