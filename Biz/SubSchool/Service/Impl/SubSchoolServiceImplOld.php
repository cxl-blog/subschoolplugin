<?php

namespace SubSchoolPlugin\Biz\Service\SubSchool\Impl;

use AppBundle\Common\ArrayToolkit;
use Biz\BaseService;
use SubSchoolPlugin\Biz\Service\SubSchool\SubSchoolServiceOld;

class SubSchoolServiceOldImpl extends BaseService implements SubSchoolServiceOld
{
//    public function createSubSchool($subSchool)
//    {
//        if (!ArrayToolkit::requireds($subSchool, array(
//            'name',
//            'address',
//            'contact',
//            'status',
//            'adminIds',
//            'phone',
//            'email',
//            'site',
//            'logo',
//            'cover',
//            'subSchoolCategoryId',
//            'branchPrincipal',
//            'principalPhone',
//            'proportion',
//            'businessLicense',
//            'applicantId',
//            'accountName',
//            'accountNumber',
//            'bankName',
//            'orgCode',
//            'orgId'
//        ))
//        ) {
//            throw $this->createServiceException('缺少必要字段，创建分校失败！');
//        }
//
//        $subSchool = ArrayToolkit::parts($subSchool, array(
//            'name',
//            'about',
//            'address',
//            'contact',
//            'status',
//            'adminIds',
//            'phone',
//            'email',
//            'site',
//            'logo',
//            'cover',
//            'subSchoolCategoryId',
//            'branchPrincipal',
//            'principalPhone',
//            'province',
//            'proportion',
//            'businessLicense',
//            'applicantId',
//            'accountName',
//            'accountNumber',
//            'bankName',
//            'orgCode',
//            'orgId'
//        ));
//        return $this->getSubSchoolDao()->addSubSchool(SubSchoolInfo::serialize($subSchool));
//    }
//
//    public function editSubSchool($id, $fields)
//    {
//        $this->tryManageSubSchool($id);
//        $subSchool = $this->getSubSchool($id);
//        if (empty($subSchool)) {
//            throw $this->createNotFoundException('分校不存在');
//        }
//
//        if (!ArrayToolkit::requireds($fields, array(
//            'name',
//            'address',
//            'contact',
//            'status',
//            'adminIds',
//            'phone',
//            'email',
//            'site',
//            'logo',
//            'cover',
//            'subSchoolCategoryId',
//            'branchPrincipal',
//            'principalPhone',
//            'proportion',
//            'businessLicense',
//            'applicantId',
//            'accountName',
//            'accountNumber',
//            'bankName',
//            'orgCode',
//        ))
//        ) {
//            throw $this->createServiceException('缺少必要字段，编辑分校失败！');
//        }
//
//        $fields = ArrayToolkit::parts($fields, array(
//            'name',
//            'about',
//            'address',
//            'contact',
//            'status',
//            'adminIds',
//            'phone',
//            'email',
//            'site',
//            'logo',
//            'cover',
//            'subSchoolCategoryId',
//            'branchPrincipal',
//            'principalPhone',
//            'province',
//            'proportion',
//            'businessLicense',
//            'applicantId',
//            'accountName',
//            'accountNumber',
//            'bankName',
//            'orgCode',
//        ));
//
//        return $this->getSubSchoolDao()->updateSubSchool($id, SubSchoolInfo::serialize($fields));
//    }
//
//    public function getSubSchool($id)
//    {
//        return SubSchoolInfo::unserialize($this->getSubSchoolDao()->getSubSchool($id));
//    }
//
//    public function openSubSchool($id)
//    {
//        $this->tryManageSubSchool($id);
//        return $this->getSubSchoolDao()->updateSubSchool($id, array('status' => 'normal'));
//    }
//
//    public function closeSubSchool($id)
//    {
//        $this->tryManageSubSchool($id);
//        return $this->getSubSchoolDao()->updateSubSchool($id, array('status' => 'disable'));
//    }
//
//    public function searchSubSchoolsCount($conditions)
//    {
//        return $this->getSubSchoolDao()->searchSubSchoolsCount($conditions);
//    }
//
////    public function searchSubSchools($conditions, $sort, $start, $limit)
////    {
////        return $this->getSubSchoolDao()->searchSubSchools($conditions, $sort, $start, $limit);
////    }
//
//    public function getSubSchoolBySchoolSite($site)
//    {
//        return $this->getSubSchoolDao()->getSubSchoolBySchoolSite($site);
//    }
//
//    public function findSubSchoolsByStatus($status)
//    {
//        return $this->getSubSchoolDao()->findSubSchoolsByStatus($status);
//    }
//
//    public function tryManageSubSchool($id)
//    {
//        if (!$this->canManageSubSchool($id)) {
//            throw $this->createAccessDeniedException('您无权操作！');
//        }
//    }
//
//    public function IsOrgIdAvailable($orgId)
//    {
//
//        if (empty($orgId)) {
//            return false;
//        }
//
//        $subSchool = $this->getSubSchoolDao()->findSubSchoolByOrgId($orgId);
//
//        return empty($subSchool) ? true : false;
//    }
//
//    public function isSiteAvailable($site)
//    {
//        return $this->getSubSchoolDao()->getSubSchoolBySchoolSite($site);
//    }
//
//    public function isSubSchoolOpen($subSchoolId)
//    {
//        if (empty($subSchoolId)) {
//            return false;
//        }
//
//        $subSchool = $this->getSubSchoolDao()->getSubSchool($subSchoolId);
//
//        return ($subSchool['status'] == 'normal') ? true : false;
//    }
//
//    public function findSubSchoolByOrgId($orgId)
//    {
//        return $this->getSubSchoolDao()->findSubSchoolByOrgId($orgId);
//    }
//
//    public function findSubSchoolsByLikeName($name)
//    {
//        return $this->getSubSchoolDao()->findSubSchoolsByLikeName($name);
//    }
//
//    protected function canManageSubSchool($id)
//    {
//        $subSchool = $this->getSubSchool($id);
//
//        if (empty($subSchool)) {
//            return false;
//        }
//
//        $user = $this->getCurrentUser();
//
//        if (!$user->isLogin()) {
//            return false;
//        }
//
//        if ($user->isAdmin()) {
//            return true;
//        }
//
//        return false;
//    }
//
//    public function getSubSchoolByApplicantIdAndStatus($applicantId, $status)
//    {
//        return SubSchoolInfo::unserialize($this->getSubSchoolDao()->getSubSchoolByApplicantIdAndStatus($applicantId, $status));
//    }
//
//    public function getSubSchoolByName($name)
//    {
//        return $this->getSubSchoolDao()->getSubSchoolByName($name);
//    }
//
//    public function createAuditRecord($field)
//    {
//        $field = ArrayToolkit::parts($field, array(
//            'name',
//            'address',
//            'contact',
//            'status',
//            'adminIds',
//            'phone',
//            'email',
//            'site',
//            'logo',
//            'cover',
//            'subSchoolCategoryId',
//            'branchPrincipal',
//            'principalPhone',
//            'province',
//            'proportion',
//            'businessLicense',
//            'applicantId',
//            'accountName',
//            'accountNumber',
//            'bankName',
//            'orgCode',
//            'orgId',
//            'schoolId',
//            'reason',
//            'result',
//            'about',
//        ));
//        $field['createdTime'] = time();
//
//        if ($field['result'] == 'success') {
//            $message = '您的入驻分校申请，审核已经通过';
//        } else {
//            $message = '您的入驻分校申请，审核未通！驳回原因：' . $field['reason'];
//        }
//
//        $this->getNotificationService()->notify($field['applicantId'], 'default', $message
//        );
//
//        return $this->getSubSchoolAuditDao()->createAuditRecord(SubSchoolInfo::serialize($field));
//    }
//
//    public function updateSubSchool($id, $field)
//    {
//        $fields = ArrayToolkit::parts($field, array(
//            'name',
//            'about',
//            'address',
//            'officePhone',
//            'adminIds',
//            'logo',
//            'cover',
//            'contact',
//            'phone',
//            'email',
//            'site',
//            'subSchoolCategoryId',
//            'recommended',
//            'recommendedSeq',
//            'recommendedTime',
//            'branchPrincipal',
//            'principalPhone',
//            'province',
//            'proportion',
//            'businessLicense',
//            'accountName',
//            'bankName',
//            'accountNumber',
//            'status',
//            'applicantId',
//            'orgCode',
//            'orgId'
//        ));
//
//        return $this->getSubSchoolDao()->updateSubSchool($id, SubSchoolInfo::serialize($fields));
//    }
//
//    public function deleteSubSchool($id)
//    {
//        return $this->getSubSchoolDao()->deleteSubSchool($id);
//    }
//
//    public function searchAuditRecordCount($conditions)
//    {
//        return $this->getSubSchoolAuditDao()->searchAuditRecordCount($conditions);
//    }
//
//    public function searchAuditRecords($conditions, $sort, $start, $limit)
//    {
//        return $this->getSubSchoolAuditDao()->searchAuditRecords($conditions, $sort, $start, $limit);
//    }
//
//    public function getAuditRecord($id)
//    {
//        return $this->getSubSchoolAuditDao()->getAuditRecord($id);
//    }
//
//    public function createSchoolUser($user)
//    {
//        return $this->getSubSchoolUserDao()->createSchoolUser($user);
//    }
//
//    public function findSchoolUsersByUserId($id)
//    {
//        return $this->getSubSchoolUserDao()->findSchoolUsersByUserId($id);
//    }
//
//    public function isUserJoined($userId, $schoolId)
//    {
//        $user = $this->getSubSchoolUserDao()->getUserByUserIdAndSchoolId($userId, $schoolId);
//
//        return empty($user) ? false : true;
//    }
//
//    public function findUserIdsBySchoolId($schoolId)
//    {
//        return $this->getSubSchoolUserDao()->findUserIdsBySchoolId($schoolId);
//    }
//
//    public function deleteSubSchoolUserByUserId($userId)
//    {
//        return $this->getSubSchoolUserDao()->deleteUserByUserId($userId);
//    }
//
//    public function findSubSchoolBySubSchoolCategoryId($categoryId)
//    {
//        return $this->getSubSchoolDao()->findSubSchoolBySubSchoolCategoryId($categoryId);
//    }
//
//    protected function getSubSchoolUserDao()
//    {
//        return $this->createDao('SubSchool:SubSchool.SubSchoolUserDao');
//    }
//
//    protected function getSubSchoolDao()
//    {
//        return $this->createDao('SubSchool:SubSchool.SubSchoolDao');
//    }
//
//    protected function getSubSchoolAuditDao()
//    {
//        return $this->createDao('SubSchool:SubSchool.SubSchoolAuditDao');
//    }
//
//    protected function getOrgService()
//    {
//        return $this->createService('Org:Org.OrgService');
//    }
//
//    protected function getNotificationService()
//    {
//        return $this->createService('User.NotificationService');
//    }
//
//}
//
//class SubSchoolInfo
//{
//    public static function serialize(array &$subSchoolInfo)
//    {
//        if (isset($subSchoolInfo['adminIds'])) {
//            if (is_array($subSchoolInfo['adminIds']) && !empty($subSchoolInfo['adminIds'])) {
//                $subSchoolInfo['adminIds'] = '|' . implode('|', $subSchoolInfo['adminIds']) . '|';
//            } else {
//                $subSchoolInfo['adminIds'] = null;
//            }
//        }
//
//        return $subSchoolInfo;
//    }
//
//    public static function unserialize(array $subSchoolInfo = null)
//    {
//        if (empty($subSchoolInfo)) {
//            return $subSchoolInfo;
//        }
//
//        if (empty($subSchoolInfo['adminIds'])) {
//            $subSchoolInfo['adminIds'] = array();
//        } else {
//            $subSchoolInfo['adminIds'] = explode('|', trim($subSchoolInfo['adminIds'], '|'));
//        }
//
//        return $subSchoolInfo;
//    }
//
//    public static function unserializes(array $subSchoolInfos)
//    {
//        return array_map(function ($subSchoolInfo) {
//            return subSchoolInfo::unserialize($subSchoolInfo);
//        }, $subSchoolInfos);
//    }
//
}