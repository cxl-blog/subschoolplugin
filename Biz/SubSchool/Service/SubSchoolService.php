<?php

namespace SubSchoolPlugin\Biz\SubSchool\Service;

interface SubSchoolService
{
    public function getSubSchool($id);

    public function countSubSchools($conditions);

    public function searchSubSchools($conditions, array $orderBy, $start, $limit);

    public function createSubSchool($fields);

    public function updateSubSchool($id, $fields);

    public function isSiteAvailable($site, $exclude);

    public function isNameAvailable($name, $exclude);

    public function closeSubSchool($id);

    public function openSubSchool($id);

    public function recommendSubSchool($id, $number);

    public function cancelRecommendSubSchool($id);

    public function tryManageSubSchool($id);

    public function getSubSchoolBySite($site);

    public function getSubSchoolByOrgId($orgId);

    public function findSubSchoolsByStatus($status);
}
