<?php

namespace SubSchoolPlugin\Biz;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class SubSchoolServiceProvider implements ServiceProviderInterface
{
    public function register(Container $biz)
    {
        $this->rewriteService($biz);
    }

    private function rewriteService($biz)
    {
        $serviceAliases = $this->getRewriteServiceAlias();
        //rewrite service
        foreach ($serviceAliases as $serviceAlias) {
            $biz["@{$serviceAlias}"] = $biz->service("SubSchoolPlugin:{$serviceAlias}");
        }
    }

    private function getRewriteServiceAlias()
    {
        return array(
            'Course:CourseSetService',
            'Classroom:ClassroomService',
            'System:SettingService'
        );
    }
}
