<?php

namespace SubSchoolPlugin\Biz\System\Service\Impl;

use Biz\System\Service\Impl\SettingServiceImpl as BaseService;

class SettingServiceImpl extends BaseService
{
    public function set($name, $value)
    {
        $namespace = $this->getNameSpace();
        $this->setByNamespace($namespace, $name, $value);
    }

    protected function getNameSpace()
    {
        try {
            $user = $this->getCurrentUser();
            if (empty($user->getSelectOrgId()) || $user->getSelectOrgId() == 1) {
                return 'default';
            }

            return 'org-'.$user->getSelectOrgId();
        } catch (\RuntimeException $e) {
            return 'default';
        }
    }
}