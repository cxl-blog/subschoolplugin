<?php
namespace SubSchool\Service\User\Impl;

use Topxia\Service\User\Impl\UserServiceImpl as BaseService;

class UserServiceImpl extends BaseService
{
    public function promoteSchoolUser($id, $number)
    {
        $user = $this->getUser($id);

        if (empty($user)) {
            throw new ResourceNotFoundException('User', $id);
        }

        $user = $this->getUserDao()->updateUser($user['id'], array('schoolPromoted' => 1, 'schoolPromotedSeq' => $number, 'schoolPromotedTime' => time()));
        $this->getLogService()->info('user', 'recommend', "推荐用户{$user['nickname']}(#{$user['id']})");
        return $user;
    }

    public function cancelSchoolPromoteUser($id)
    {
        $user = $this->getUser($id);

        if (empty($user)) {
            throw new ResourceNotFoundException('User', $id);
        }

        $user = $this->getUserDao()->updateUser($user['id'], array('schoolPromoted' => 0, 'schoolPromotedSeq' => 0, 'schoolPromotedTime' => 0));

        $this->getLogService()->info('user', 'cancel_recommend', sprintf('取消推荐用户%s(#%u)', $user['nickname'], $user['id']));
        return $user;
    }
} 