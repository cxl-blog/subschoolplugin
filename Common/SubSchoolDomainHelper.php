<?php

namespace SubSchoolPlugin\Common;

use Symfony\Component\HttpFoundation\Request;
use Topxia\Service\Common\ServiceKernel;

class SubSchoolDomainHelper
{
    const HOST_NAME = 'www';

    public static function getHost(Request $request)
    {
        return explode('.', $request->getHost());
    }

    public static function getSubName(Request $request)
    {
        $host = $request->getHost();
        $customDomain = self::getKernel()->getParameter('custom_domain');
        $customDomain = trim($customDomain, '.');
        $subDomainName = str_replace($customDomain, '', $host);
        $subDomainName = trim($subDomainName, '.');
        return $subDomainName ? $subDomainName : self::HOST_NAME;
    }

    public static function getPathInfo(Request $request)
    {
        return $request->getPathInfo();
    }

    public static function isAdminPath(Request $request)
    {
        $pathinfo = self::getPathInfo($request);
        $path = explode('/', $pathinfo);
        if ($path[1] == 'admin') {
            return true;
        }
        return false;
    }

    public static function getScheme(Request $request)
    {
        return $request->getScheme();
    }

    public static function buildAdminPath(Request $request, $site)
    {
        $path = self::getPathInfo($request);
        $domain = self::getKernel()->getParameter('custom_domain');
        $scheme = self::getScheme($request);
        if (empty($site)) {
            $site = self::HOST_NAME;
        }

        $url = $scheme . '://' . $site . $domain . $path;
        return $url;
    }

    /**
     * @return 考虑剔除
     */

    public static function getHostName()
    {
        $customDomain = self::getKernel()->getParameter('custom_domain');
        $host = explode('.', $customDomain);
        //unset($host[0]);
        return implode('.', $host);
    }

    public static function isMainSite(Request $request)
    {
        $host = explode('.', $request->getHost());
        if ($host[0] == self::HOST_NAME || $host[0] == self::getMainHostName()) {
            return true;
        } else {
            return false;
        }
    }

    public static function buildMainSiteUrl(Request $request)
    {
        return $request->getScheme() . '://' . self::HOST_NAME . self::getKernel()->getParameter('custom_domain');
    }

    public static function getMainHostName()
    {

        $customDomain = self::getKernel()->getParameter('custom_domain');
        $customDomain = explode('.', $customDomain);
        return $customDomain[0];
    }

    public static function buildSiteUrlBySiteName($siteName)
    {
        if (empty($siteName)) {
            $siteName = 'www';
        }
        return 'http://' . $siteName . self::getKernel()->getParameter('custom_domain');
    }

    protected static function getKernel()
    {
        return ServiceKernel::instance();
    }

    public function getSubSchoolService()
    {
        return ServiceKernel::instance()->createService('SubSchool:SubSchool.SubSchoolService');
    }
}
