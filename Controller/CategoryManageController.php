<?php
namespace SubSchoolPlugin\Controller;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;
use AppBundle\Common\TreeToolkit;

class CategoryManageController extends BaseController
{
   public function sortCategoryAction(Request $request){
        $ids = $request->request->get('ids');

        if (!empty($ids)) {
             $this->getCategoryService()->sortCategories($ids);
        }

        return $this->createJsonResponse(true);
   }
    public function indexAction()
    {
        $categories = $this->getCategoryService()->getCategoryTree();
        $categories = TreeToolkit::makeTree($categories, 'seq');
        return $this->render('SubSchoolPlugin:CategoryManage:index.html.twig', array(
            'categories' => $categories
        ));
    }

    public function createCategoryAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $category = $request->request->all();
            $this->getCategoryService()->createCategory($category);

            return $this->createJsonResponse(true);
        }
        $category = array(
            'parentId' => $request->query->get('parentId', 0),
        );

        return $this->render('SubSchoolPlugin:CategoryManage:modal.html.twig', array(
            'category' => $category
        ));
    }

    public function editCategoryAction(Request $request, $id)
    {
        $category = $this->getCategoryService()->getCategory($id);
        if (empty($category)) {
            throw $this->createNotFoundException('找不到该分校分类');
        }

        if ($request->getMethod() == 'POST') {
            $category = $request->request->all();
            $this->getCategoryService()->updateCategory($id, $category);

            return $this->createJsonResponse(true);
        }

        return $this->render('SubSchoolPlugin:CategoryManage:modal.html.twig', array(
            'category' => $category
        ));
    }

    public function deleteCategoryAction(Request $request, $id)
    {
        $category = $this->getCategoryService()->getCategory($id);
        if (empty($category)) {
            return $this->createNotFoundException();
        }

        $school = $this->getSubSchoolService()->findSubSchoolBySubSchoolCategoryId($id);
        if(!empty($school)){
            return $this->createJsonResponse(array('status' => 'error', 'message' => '该分类下还有分校, 请先移除该分类下的所有分校！！！'));
        }

        $children = $this->getCategoryService()->findCategoriesByParentId($id);
        if (!empty($children)) {
           return $this->createJsonResponse(array('status' => 'error', 'message' => '该分类下还有子分类, 请先删除子分类'));
        }

        $this->getCategoryService()->deleteCategory($id);

        return $this->createJsonResponse(array('status' => 'success', 'message' => '分类已删除'));
    }

    public function checkCategoryAction(Request $request)
    {
        $value   = $request->query->get('value');
        $exclude = $request->query->get('exclude');

        $conditions = array('code' => $value);
        $orderBy  = array('createdTime' => 'DESC');
        $category = $this->getCategoryService()->searchCategories($conditions, $orderBy, 0, 1);
        if (empty($category) || $value == $exclude) {
            $response = array('success' => true, 'message' => '');
        } else {
            $message  = "该编码已被其他分类使用";
            $response = array('success' => false, 'message' => $message);
        }

        return $this->createJsonResponse($response);
    }

    public function checkCategoryNameAction(Request $request)
    {
        $value   = $request->query->get('value');
        $exclude = $request->query->get('exclude');

        $conditions = array('name' => $value);
        $orderBy  = array('createdTime' => 'DESC');
        $category = $this->getCategoryService()->searchCategories($conditions, $orderBy, 0, 1);
        if (empty($category) || $value == $exclude) {
            $response = array('success' => true, 'message' => '');
        } else {
            $message  = "该分类名称已被其他分类使用";
            $response = array('success' => false, 'message' => $message);
        }

        return $this->createJsonResponse($response);
    }

    public function checkCategoryParentIdAction(Request $request)
    {
        $selectedParentId = $request->query->get('value');
        $currentId = $request->query->get('currentId');
        if ($currentId == $selectedParentId && 0 != $selectedParentId) {
            $response = array('success' => false, 'message' => '不能选择自己作为父分类');
        } else {
            $response = array('success' => true, 'message' => '');
        }

        return $this->createJsonResponse($response);
    }

    /**
     * @return SubSchoolCategoryServiceImpl
     */

    protected function getCategoryService()
    {
        return $this->createService('SubSchoolPlugin:SubSchool:CategoryService');
    }

    /**
     * @return SubSchoolServiceImpl
     */

    protected function getSubSchoolService()
    {
        return $this->createService('SubSchoolPlugin:SubSchool:SubSchoolService');
    }
}
