<?php

namespace SubSchoolPlugin\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\BaseController;
use AppBundle\Common\ArrayToolkit;

class CourseManageController extends BaseController
{
    public function copyCourseToOrgAction(Request $request, $courseId)
    {
        $subSchools = $this->getSubSchoolService()->findSubSchoolsByStatus('normal');
        $subSchools = ArrayToolkit::index($subSchools, 'orgId');

        $courseSet = $this->getCourseSetService()->getCourseSet($courseId);
        unset($subSchools[$courseSet['orgId']]);
        if ($request->getMethod() == 'POST') {
            $data =$request->request->all();
            $orgIds =empty($data['orgIds']) ? array() : $data['orgIds'];
            $subSchoolOrgIds = ArrayToolkit::column($subSchools, 'orgId');
            $orgIds = array_intersect($orgIds, $subSchoolOrgIds);
            $this->copyCourseAndChangeOrgInfo($orgIds, $courseId);

            return $this->redirect($this->generateUrl('admin_course_set'));
        }
        return $this->render('SubSchoolPlugin:MoveCourse:index.html.twig', array(
            'courseId' => $courseId,
            'subSchools' => $subSchools
        ));
    }

    public function copyCourseToFullSiteAction(Request $request, $courseId)
    {
        if ($request->getMethod() == 'POST') {
            $orgIds = array(1);
            $this->copyCourseAndChangeOrgInfo($orgIds, $courseId);
            return $this->redirect($this->generateUrl('admin_course_set'));
        }
        $courseSet = $this->getCourseSetService()->getCourseSet($courseId);
        return $this->render('SubSchoolPlugin:MoveCourse:comfirm-modal.html.twig', array(
            'course' => $courseSet,
        ));
    }

    private function copyCourseAndChangeOrgInfo($orgIds, $courseId)
    {
        $orginCourse = $this->getCourseSetService()->getCourseSet($courseId);
        $conditions = array(
            'title' => $orginCourse['title'],
            'parentId' => 0,
            'likeAllOrgCode' => '1.'
        );
        $existedCourses = $this->getCourseSetService()->searchCourseSets(
            $conditions,
            array('createdTime' => 'DESC'),
            0,
            PHP_INT_MAX
        );

        $existedOrgCodes = ArrayToolkit::column($existedCourses, 'orgCode');
        $orgs = $this->getOrgService()->findOrgsByIds($orgIds);
        $orgCodes = ArrayToolkit::column($orgs, 'orgCode');
        $orgCodes = array_diff($orgCodes, $existedOrgCodes);

        foreach ($orgCodes as $orgCode) {
            $this->getCourseSetService()->copyCourseSetToOrgCode($courseId, $orgCode);
        }
    }

    protected function getCourseSetService()
    {
        return $this->createService('Course:CourseSetService');
    }

    protected function getSubSchoolService()
    {
        return $this->createService('SubSchoolPlugin:SubSchool:SubSchoolService');
    }

    protected function getOrgService()
    {
        return $this->createService('Org:OrgService');
    }

    protected function getWebExtension()
    {
        return $this->container->get('topxia.twig.web_extension');
    }
}
