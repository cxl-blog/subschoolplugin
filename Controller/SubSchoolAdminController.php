<?php
namespace SubSchoolPlugin\Controller;

use SubSchoolPlugin\Biz\SubSchool\Service\CategoryService;
use SubSchoolPlugin\Biz\SubSchool\Service\SubSchoolService;
use SubSchoolPlugin\Common\SubSchoolDomainHelper;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;
use AppBundle\Controller\Admin\BaseController;

class SubSchoolAdminController extends BaseController
{
    public function indexAction(Request $request)
    {
        $conditions = $request->query->all();
        $count = $this->getSubSchoolService()->countSubSchools($conditions);
        $paginator = new Paginator(
            $request,
            $count,
            20
        );
        $subSchools = $this->getSubSchoolService()->searchSubSchools(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $categories = $this->getCategoryService()->searchCategories(array(), array(), 0, 999);

        // @todo 需要重构SubSchoolDomainHelper
        $hostName = SubSchoolDomainHelper::getHostName();

        return $this->render('SubSchoolPlugin:subschool-admin:index.html.twig', array(
            'paginator' => $paginator,
            'subSchools' => $subSchools,
            'hostName' => $hostName,
            'categories' => ArrayToolkit::index($categories, 'id')
        ));
    }

    public function createAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $subSchool = $request->request->all();
            $subSchool['status'] = 'normal';

            if(empty($subSchool['categoryId'])) {
                $subSchool['categoryId'] = NULL;
            }

            $this->getSubSchoolService()->createSubSchool($subSchool);

            return $this->redirect($this->generateUrl('admin_subschool'));
        }


        $categories = $this->getCategoryService()->getCategoryTree();
        $categories = $this->makeDepthTree($categories);
        $hostName = SubSchoolDomainHelper::getHostName();

        return $this->render('SubSchoolPlugin:subschool-admin:create.html.twig', array(
            'categories' => $categories,
            'hostName' => $hostName,
        ));
    }

    /**
     * @todo
     */
    public function userMatchAction(Request $request)
    {
        $likeString = $request->query->get('q');
        $users = $this->getUserService()->searchUsers(
            array('nickname' => $likeString),
            array('id' => 'DESC'),
            0,
            10
        );

        $admins = array();

        foreach ($users as $user) {
            if (in_array('ROLE_SUPER_ADMIN', $user['roles']) || in_array('ROLE_ADMIN', $user['roles']) || in_array('ROLE_SCHOOL_ADMIN', $user['roles'])) {
                continue;
            }
            $admins[] = array(
                'adminId' => $user['id'],
                'nickname' => $user['nickname'],
            );
        }

        return $this->createJsonResponse($admins);
    }

    /**
     * @todo
     */
    public function editAction(Request $request, $id)
    {
        $subSchool = $this->getSubSchoolService()->getSubSchool($id);
        if (empty($subSchool)) {
            throw $this->createNotFoundException('分校不存在');
        }

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $fields['status'] = 'normal';

            if(empty($fields['categoryId'])) {
                $fields['categoryId'] = NULL;
            }

            $subSchool = $this->getSubSchoolService()->updateSubSchool($subSchool['id'], $fields);

            return $this->redirect($this->generateUrl('admin_subschool'));
        }

        $users = $this->getUserService()->findUsersByIds($subSchool['adminIds']);
        foreach ($users as $key => $user) {
            $users[$key]['adminId'] = $user['id'];
        }

        $categories = $this->getCategoryService()->getCategoryTree();
        $categories = $this->makeDepthTree($categories);
        $hostName = SubSchoolDomainHelper::getHostName($request);

        return $this->render('SubSchoolPlugin:subschool-admin:create.html.twig', array(
            'subSchool' => $subSchool,
            'categories' => $categories,
            'hostName' => $hostName,
            'admins' => $users,
        ));
    }

    /**
     * @todo
     */
    public function closeAction(Request $request, $id)
    {
        $subSchool = $this->getSubSchoolService()->getSubSchool($id);
        if (empty($subSchool)) {
            throw $this->createNotFoundException('分校不存在');
        }

        $subSchool = $this->getSubSchoolService()->closeSubSchool($id);
        $hostName = SubSchoolDomainHelper::getHostName($request);

        return $this->render('SubSchoolPlugin:subschool-admin:index-tr.html.twig', array(
            'subSchool' => $subSchool,
            'hostName'  => $hostName
        ));
    }

    /**
     * @todo
     */
    public function openAction(Request $request, $id)
    {
        $subSchool = $this->getSubSchoolService()->getSubSchool($id);
        if (empty($subSchool)) {
            throw $this->createNotFoundException('分校不存在');
        }

        $subSchool = $this->getSubSchoolService()->openSubSchool($id);
        $hostName = SubSchoolDomainHelper::getHostName($request);

        return $this->render('SubSchoolPlugin:subschool-admin:index-tr.html.twig', array(
            'subSchool' => $subSchool,
            'hostName'  => $hostName
        ));
    }

    /**
     * @todo
     */
    public function recommendAction(Request $request, $id)
    {
        $subSchool = $this->getSubSchoolService()->getSubSchool($id);
        if (empty($subSchool)) {
            throw $this->createNotFoundException('分校不存在');
        }

        if ($request->getMethod() == 'POST') {
            $number = $request->request->get('number');

            $subSchool = $this->getSubSchoolService()->recommendSubSchool($id, $number);

            return $this->render('SubSchoolPlugin:subschool-admin:index-tr.html.twig', array(
                'subSchool' => $subSchool,
                'hostName'  => SubSchoolDomainHelper::getHostName($request)
            ));
        }
        return $this->render('SubSchoolPlugin:subschool-admin:recommend-modal.html.twig', array(
            'subSchool' => $subSchool,
        ));
    }

    /**
     * @todo
     */
    public function recommendCancelAction(Request $request, $id)
    {
        $subSchool = $this->getSubSchoolService()->getSubSchool($id);
        if (empty($subSchool)) {
            throw $this->createNotFoundException('分校不存在');
        }

        $subSchool = $this->getSubSchoolService()->cancelRecommendSubSchool($id);
        $hostName = SubSchoolDomainHelper::getHostName($request);

        return $this->render('SubSchoolPlugin:subschool-admin:index-tr.html.twig', array(
            'subSchool' => $subSchool,
            'hostName'  => $hostName
        ));
    }

    /**
     * @todo
     */
    protected function makeDepthTree($categories)
    {
        $indent = '　';
        foreach ($categories as $category) {
            $choices[$category['id']] = str_repeat($indent, ($category['depth'] - 1)) . $category['name'];
        }

        return $choices;
    }

    /**
     * @todo
     * @return SubSchoolService
     */
    protected function getSubSchoolService()
    {
        return $this->createService('SubSchoolPlugin:SubSchool:SubSchoolService');
    }

    /**
     * @todo
     * @return CategoryService
     */

    protected function getCategoryService()
    {
        return $this->createService('SubSchoolPlugin:SubSchool:CategoryService');
    }

    /**
     * @todo
     */
    protected function getOrgService()
    {
        return $this->createService('Org:OrgService');
    }

    /**
     * @todo
     */
    protected function getSettingService()
    {
        return $this->createService('System:SettingService');
    }
}
