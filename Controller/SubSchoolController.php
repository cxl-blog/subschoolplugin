<?php
namespace SubSchoolPlugin\Controller;

use SubSchoolPlugin\Common\SubSchoolDomainHelper;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;
use AppBundle\Controller\BaseController;

class SubSchoolController extends BaseController
{
    public function uploadAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $files = $request->request->all();
            return $this->createJsonResponse($files);
        }

        return $this->render('SubSchoolPlugin:SubSchool:upload-modal.html.twig', array(
            'pictureUrl' => "",
        ));
    }

    public function cropAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $options = $request->request->all();
            $files = $this->changeCoverPicture($options["images"]);

            foreach ($files as $key => $file) {
                $files[$key]["file"]['url'] = $this->get('web.twig.extension')->getFilePath($file["file"]['uri']);
            }

            return $this->createJsonResponse($files);
        }

        $fileId = $request->getSession()->get("fileId");
        list($pictureUrl, $naturalSize, $scaledSize) = $this->getFileService()->getImgFileMetaInfo($fileId, 516, 344);

        return $this->render('SubSchoolPlugin:SubSchool:picture-crop-modal.html.twig', array(
            'pictureUrl' => $pictureUrl,
            'naturalSize' => $naturalSize,
            'scaledSize' => $scaledSize,
        ));
    }

    public function changeCoverPicture($data)
    {
        if (!is_array($data)) {
            return array($data);
        }

        $fileIds = ArrayToolkit::column($data, "id");
        $files = $this->getFileService()->getFilesByIds($fileIds);

        $data = ArrayToolkit::index($data, "type");
        $files = ArrayToolkit::index($files, "id");

        foreach ($data as $key => $value) {
            if ($key == "origin") {
                $file = $this->getFileService()->getFileObject($value["id"]);
                $file = $this->getFileService()->uploadFile("article", $file);
                $data[$key]["file"] = $file;

                $this->getFileService()->deleteFileByUri($files[$value["id"]]["uri"]);
            } else {
                $data[$key]["file"] = $files[$value["id"]];
            }
        }

        return $data;
    }

    public function siteCheckAction(Request $request)
    {
        $site = $request->query->get('value');
        $exclude = $request->query->get('exclude');

        if ($site == 'www') {
            return $this->createJsonResponse(array('success' => false, 'message' => '二级域名不能以www开头！'));
        }

        $isAvailable = $this->getSubSchoolService()->isSiteAvailable($site, $exclude);

        if (!$isAvailable) {
            return $this->createJsonResponse(array('success' => false, 'message' => '该分校域名已存在'));
        }

        return $this->createJsonResponse(array('success' => true, 'message' => ''));
    }

    public function nameCheckAction(Request $request)
    {
        $value = $request->query->get('value');
        $exclude = $request->query->get('name');

        $isAvailable = $this->getSubSchoolService()->isNameAvailable(trim($value), $exclude);

        if (!$isAvailable) {
            return $this->createJsonResponse(array('success' => false, 'message' => "您输入的分校名称已经存在，请重新输入"));
        }

        return $this->createJsonResponse(array('success' => true, 'message' => ""));
    }

    public function exploreAction(Request $request, $category)
    {
        $user = $this->getCurrentUser();
        $categoryArray = array();
        $province = $request->query->get('province');
        $orderBy = $request->query->get('orderBy');
        $conditions = array();

        $conditions['code'] = $category;

        if (!empty($conditions['code'])) {
            $categoryArray = $this->getSubSchoolCategoryService()->getCategoryByCode($conditions['code']);
            $childrenIds = $this->getSubSchoolCategoryService()->findCategoryChildrenIds($categoryArray['id']);
            $categoryIds = array_merge($childrenIds, array($categoryArray['id']));
            $conditions['categoryIds'] = $categoryIds;
        }
        unset($conditions['code']);

        if (!empty($province)) {
            $conditions['province'] = $province;
        }

        $conditions['status'] = 'normal';

        $subSchoolCount = $this->getSubSchoolService()->countSubSchools($conditions);
        $paginator = new Paginator($request, $subSchoolCount, 8);

        if (isset($orderBy) && $orderBy == 'latest') {
            $searchOrderBy = array('createdTime' => 'DESC');
            $subSchools = $this->getSubSchoolService()->searchSubSchools(
                $conditions,
                $searchOrderBy,
                $paginator->getOffsetCount(),
                $paginator->getPerPageCount()
            );
        } else {
            $searchOrderBy = array('recommendedSeq' => 'ASC');
            $conditions['recommended'] = 1;
            $recommendCount = $this->getSubSchoolService()->countSubSchools($conditions);
            $currentPage = $request->query->get('page') ? $request->query->get('page') : 1;
            $recommendPage = intval($recommendCount / 8);
            $recommendLeft = $recommendCount % 8;

            if ($currentPage <= $recommendPage) {
                $subSchools = $this->getSubSchoolService()->searchSubSchools(
                    $conditions,
                    $searchOrderBy,
                    ($currentPage - 1) * 8,
                    8
                );
            } elseif (($recommendPage + 1) == $currentPage) {
                $subSchools = $this->getSubSchoolService()->searchSubSchools(
                    $conditions,
                    $searchOrderBy,
                    ($currentPage - 1) * 8,
                    8
                );
                $conditions['recommended'] = 0;
                $branchSchoolsTemp = $this->getSubSchoolService()->searchSubSchools(
                    $conditions,
                    array('createdTime' => 'DESC'),
                    0,
                    8 - $recommendLeft
                );

                $subSchools = array_merge($subSchools, $branchSchoolsTemp);
            } else {
                $conditions['recommended'] = 0;
                $subSchools = $this->getSubSchoolService()->searchSubSchools(
                    $conditions,
                    array('createdTime' =>  'DESC'),
                    (8 - $recommendLeft) + ($currentPage - $recommendPage - 2) * 8,
                    8
                );
            }
        }

        $hostName = SubSchoolDomainHelper::getHostName($request);
        return $this->render('SubSchoolPlugin:Explore:explore.html.twig', array(
            'category' => $category,
            'subSchools' => $subSchools,
            'province' => $province,
            'paginator' => $paginator,
            'orderBy' => $orderBy,
            'hostName' => $hostName
        ));
    }

    public function treeNavAction(Request $request, $category, $path, $orderBy)
    {
        list($rootCategories, $categories, $activeIds) = $this->getSubSchoolCategoryService()->makeNavCategories($category);

        return $this->render("SubSchoolPlugin:Explore:explore-nav.html.twig", array(
            'rootCategories' => $rootCategories,
            'categories'     => $categories,
            'category'       => $category,
            'activeIds'      => $activeIds,
            'path'           => $path,
            'orderBy'        => $orderBy
        ));
    }

    protected function getFileService()
    {
        return $this->createService('Content:FileService');
    }

    protected function getSubSchoolService()
    {
        return $this->createService('SubSchoolPlugin:SubSchool:SubSchoolService');
    }

    /**
     * @return SubSchoolCategoryServiceImpl
     */

    protected function getSubSchoolCategoryService()
    {
        return $this->createService('SubSchoolPlugin:SubSchool:CategoryService');
    }
}
