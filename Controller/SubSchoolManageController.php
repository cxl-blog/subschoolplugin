<?php
namespace SubSchoolPlugin\Controller;


use SubSchoolPlugin\Common\SubSchoolDomainHelper;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;
use AppBundle\Controller\Admin\BaseController;


class SubSchoolManageController extends BaseController
{
    public function indexAction(Request $request)
    {
        $conditions = $request->query->all();
        $conditions['allStatus'] = array('normal', 'disable');
        $name = $request->query->get('name');
        $user = $this->getCurrentUser();
        $count = $this->getSubSchoolService()->countSubSchools($conditions);
        $paginator = new Paginator(
            $request,
            $count,
            20
        );

        $subSchools = $this->getSubSchoolService()->searchSubSchools(
            $conditions,
            array('createdTime', 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $categories = $this->getSubSchoolCategoryService()->findCategoriesByIds(ArrayToolkit::column($subSchools, 'subSchoolCategoryId'));

        $orgs = $this->getOrgService()->findOrgsByIds(ArrayToolkit::column($subSchools, 'orgId'));
        $hostName = SubDomainHelper::getHostName($request);
        return $this->render('SubSchoolBundle:SubSchoolManage:index.html.twig', array(
            'paginator' => $paginator,
            'subSchools' => $subSchools,
            'categories' => $categories,
            'orgs' => $orgs,
            'hostName' => $hostName,
            'name' => $name
        ));
    }

    public function createAction(Request $request)
    {
        $currentUser = $this->getCurrentUser();
        $site = $this->getSettingService()->get('site', array());
        $site = $this->setting('site');
        if ($request->getMethod() == 'POST') {
            $subSchool = $request->request->all();
            $subSchool['status'] = 'normal';
            $subSchool['applicantId'] = $currentUser['id'];
            $subSchool['name'] = $subSchool['schoolname'];

            unset($subSchool['schoolname']);

            $this->getSubSchoolCreateService()->createSubSchool($subSchool, 'create');
        }

        $categories = $this->getSubSchoolCategoryService()->getCategoryTree();
        $indent = '　';
        foreach ($categories as $category) {
            $choices[$category['id']] = str_repeat($indent, ($category['depth'] - 1)) . $category['name'];
        }
        $hostName = SubDomainHelper::getHostName($request);
        return $this->render('SubSchoolBundle:SubSchoolManage:create.html.twig', array(
            'categories' => $choices,
            'hostName' => $hostName,
        ));
    }

    public function editAction(Request $request, $id)
    {
        $currentUser = $this->getCurrentUser();
        $subSchool = $this->getSubSchoolService()->getSubSchool($id);
        if (empty($subSchool)) {
            throw $this->createNotFoundException('分校不存在');
        }

        if ($subSchool['status'] == "auditing") {
            throw $this->createNotFoundException('分校不存在');
        }

        $adminIds = $subSchool['adminIds'];
        $admins = array();

        foreach ($adminIds as $adminId) {
            $user = $this->getUserService()->getUser($adminId);
            $admins[] = array(
                'adminId' => $adminId,
                'nickname' => $user['nickname'],
            );
        }

        $site = $this->getSettingService()->get('site', array());
        if ($request->getMethod() == 'POST') {
            $subSchoolFields = $request->request->all();
            $subSchoolFields['status'] = 'normal';
            $subSchoolFields['applicantId'] = $currentUser['id'];
            $subSchoolFields['name'] = $subSchoolFields['schoolname'];
            unset($subSchoolFields['schoolname']);

            $org['name'] = $subSchoolFields['name'];
            $org['code'] = $subSchoolFields['site'];
            $org['parentId'] = 1;
            $org = $this->getOrgService()->updateOrg($subSchool['orgId'], $org);

            $site = array(
                'name' => $subSchoolFields['name'],
                'slogan' => '',
                'logo' => $subSchoolFields['logo']
            );

            $this->getSettingService()->setByNamespace('org-' . $subSchool['orgId'], 'site', $site);
            $this->getLogService()->info('system', 'update_settings', "更新站点设置", $site);

            $subSchoolFields['orgCode'] = $org['orgCode'];
            if (!isset($subSchoolFields['adminIds']) || empty($subSchoolFields['adminIds'])) {
                $subSchoolFields['adminIds'] = $adminIds;
            }
            if (count($subSchoolFields['adminIds']) > count($subSchool['adminIds'])) {
                $adminDiff = array_diff($subSchoolFields['adminIds'], $subSchool['adminIds']);
            } elseif (count($subSchoolFields['adminIds']) < count($subSchool['adminIds'])) {
                $adminDiff = array_diff($subSchool['adminIds'], $subSchoolFields['adminIds']);
            } else {
                $adminDiffOne = array_diff($subSchoolFields['adminIds'], $subSchool['adminIds']);
                $adminDiffTwo = array_diff($subSchool['adminIds'], $subSchoolFields['adminIds']);
                $adminDiff = array_merge($adminDiffOne, $adminDiffTwo);
            }

            if (!empty($adminDiff)) {
                foreach ($subSchoolFields['adminIds'] as $adminUserId) {
                    $this->getUserService()->changeUserOrg($adminUserId, $subSchoolFields['orgCode']);
                    $roles = array_merge(array("ROLE_SCHOOL_ADMIN"), $this->getUserService()->getUser($adminUserId)['roles']);

                    $this->getUserService()->changeUserRoles($adminUserId, array_unique($roles));
                }

                foreach ($adminDiff as $diffId) {
                    if (in_array($diffId , $subSchool['adminIds'])) {
                        $adminRoles = $this->getUserService()->getUser($diffId)['roles'];
                        foreach ($adminRoles as $key => $value) {
                            if ($value == "ROLE_SCHOOL_ADMIN") {
                                unset($adminRoles[$key]);
                            }
                        }
                        $this->getUserService()->changeUserRoles($diffId, array_unique($adminRoles));
                    }
                }
            }
            $subSchoolResult = $this->getSubSchoolService()->editSubSchool($id, $subSchoolFields);
            $this->getLogService()->info('sub_school', 'edit_sub_school', "管理员编辑分校({$subSchoolResult['name']}#{$subSchoolResult['id']})", $subSchoolResult);
        }

        $categories = $this->getSubSchoolCategoryService()->getCategoryTree();
        $indent = '　';
        foreach ($categories as $category) {
            $choices[$category['id']] = str_repeat($indent, ($category['depth'] - 1)) . $category['name'];
        }

        $hostName = SubDomainHelper::getHostName($request);
        return $this->render('SubSchoolBundle:SubSchoolManage:create.html.twig', array(
            'subSchool' => $subSchool,
            'categories' => $choices,
            'hostName' => $hostName,
            'admins' => $admins,
        ));
    }

    public function applyAction(Request $request)
    {
        $currentUser = $this->getCurrentUser();

        if (!$currentUser->isLogin()) {
            return $this->createMessageResponse('info', '请先登录系统', '', 3, $this->generateUrl('login'));
        }

        if ($currentUser->isAdmin() && !in_array('ROLE_SCHOOL_ADMIN', $currentUser['roles'])) {
            return $this->redirect($this->generateUrl('admin_sub_school_create'));
        }
        if (in_array('ROLE_SCHOOL_ADMIN', $currentUser['roles'])) {
            return $this->redirect($this->generateUrl('sub_school_nitification', array('status' => 'false')));
        }

        if ($request->getMethod() == 'POST') {
            $subSchool = $request->request->all();
            $subSchool['status'] = 'auditing';
            $subSchool['adminIds'] = array();
            $subSchool['applicantId'] = $currentUser['id'];
            $subSchool['name'] = $subSchool['schoolname'];
            unset($subSchool['schoolname']);

            $subSchool['orgId'] = 0;
            $subSchool['orgCode'] = '0.';

            $subSchool = $this->getSubSchoolService()->createSubSchool($subSchool);
            $this->getLogService()->info('sub_school', 'apply_sub_school', "用户申请分校({$subSchool['name']}#{$subSchool['id']})", $subSchool);
        }

        $subSchool = $this->getSubSchoolService()->getSubSchoolByApplicantIdAndStatus($currentUser['id'], "auditing");

        if ($subSchool != null) {
            return $this->redirect($this->generateUrl('sub_school_nitification', array('status' => 'true')));
        }

        $categories = $this->getSubSchoolCategoryService()->getCategoryTree();
        $indent = '　';
        foreach ($categories as $category) {
            $choices[$category['id']] = str_repeat($indent, ($category['depth'] - 1)) . $category['name'];
        }

        $hostName = SubDomainHelper::getHostName($request);
        return $this->render('SubSchoolBundle:SubSchoolManage:apply.html.twig', array(
            'categories' => $choices,
            'hostName' => $hostName,
        ));
    }

    public function openAction(Request $request, $id)
    {
        // $subSchool = $this->getSubSchoolService()->openSubSchool($id);
        // if ($subSchool) {
        //     $this->getLogService()->info('sub_school', 'open_sub_school', "管理员开启分校({$subSchool['name']}#{$subSchool['id']})", $subSchool);
        // }
        // if (!$subSchool) {
        //     $this->getLogService()->error('sub_school', 'open_sub_school', "管理员开启分校(#{$subSchool['id']})失败");
        //     throw $this->createAccessDeniedException();
        // }
        // return $this->createJsonResponse(true);
    }

    public function closeAction(Request $request, $id)
    {
        // $subSchool = $this->getSubSchoolService()->closeSubSchool($id);
        // if ($subSchool) {
        //     $this->getLogService()->info('sub_school', 'close_sub_school', "管理员关闭分校({$subSchool['name']}#{$subSchool['id']})", $subSchool);
        // }
        // if (!$subSchool) {
        //     $this->getLogService()->error('sub_school', 'close_sub_school', "管理员关闭分校(#{$subSchool['id']})失败");
        //     throw $this->createAccessDeniedException();
        // }
        // return $this->createJsonResponse(true);

    }

    public function orgCheckAction(Request $request)
    {
        $orgId = $request->query->get('orgId');
        $available = $this->getSubSchoolService()->IsOrgIdAvailable($orgId);
        if ($available) {
            return $this->createJsonResponse(true);
        } else {
            return $this->createJsonResponse(false);
        }
    }

    private function getOrgList()
    {
        $conditions = array(
            'parentId' => 1,
            'depth' => 2,
        );
        $orgLists = $this->getOrgService()->searchOrgs(
            $conditions,
            array('seq', 'ASC'),
            0,
            PHP_INT_MAX
        );
        return $orgLists;
    }

    // public function isSubNameRepetAction(Request $request)
    // {
    //     $value = $request->query->get('value');
    //     $name = $request->query->get('name');

    //     if ($value == $name) {
    //         return $this->createJsonResponse(array('success' => true, 'message' => ""));
    //     }
    //     $SubSchool = $this->getSubSchoolService()->getSubSchoolByName(trim($value));

    //     if ($SubSchool != null) {
    //         $data = array('success' => false, 'message' => "您输入的分校名称已经存在，请重新输入");
    //     } else {
    //         $data = array('success' => true, 'message' => "");
    //     }
    //     return $this->createJsonResponse($data);
    // }

    // public function uploadAction(Request $request)
    // {
    //     if ($request->getMethod() == 'POST') {
    //         $files = $request->request->all();
    //         return $this->createJsonResponse($files);
    //     }

    //     return $this->render('SubSchoolBundle:SubSchoolManage:upload-modal.html.twig', array(
    //         'pictureUrl' => "",
    //     ));
    // }

    // public function cropAction(Request $request)
    // {
    //     if ($request->getMethod() == 'POST') {
    //         $options = $request->request->all();
    //         $files = $this->changeIndexPicture($options["images"]);

    //         foreach ($files as $key => $file) {
    //             $files[$key]["file"]['url'] = $this->get('topxia.twig.web_extension')->getFilePath($file["file"]['uri']);
    //         }

    //         return $this->createJsonResponse($files);
    //     }

    //     $fileId = $request->getSession()->get("fileId");
    //     list($pictureUrl, $naturalSize, $scaledSize) = $this->getFileService()->getImgFileMetaInfo($fileId, 516, 344);

    //     return $this->render('SubSchoolBundle:SubSchoolManage:picture-crop-modal.html.twig', array(
    //         'pictureUrl' => $pictureUrl,
    //         'naturalSize' => $naturalSize,
    //         'scaledSize' => $scaledSize,
    //     ));
    // }

    // public function adminsMatchAction(Request $request)
    // {
    //     $likeString = $request->query->get('q');
    //     $users = $this->getUserService()->searchUsers(
    //         array('nickname' => $likeString),
    //         array('id', 'DESC'),
    //         0, 10
    //     );

    //     $admins = array();

    //     foreach ($users as $user) {
    //         if (in_array('ROLE_SUPER_ADMIN', $user['roles']) || in_array('ROLE_ADMIN', $user['roles']) || in_array('ROLE_SCHOOL_ADMIN', $user['roles'])) {
    //             continue;
    //         }
    //         $admins[] = array(
    //             'adminId' => $user['id'],
    //             'nickname' => $user['nickname'],
    //         );
    //     }

    //     return $this->createJsonResponse($admins);
    // }

    // public function changeIndexPicture($data)
    // {
    //     if (!is_array($data)) {
    //         return array($data);
    //     }

    //     $fileIds = ArrayToolkit::column($data, "id");
    //     $files = $this->getFileService()->getFilesByIds($fileIds);

    //     $data = ArrayToolkit::index($data, "type");
    //     $files = ArrayToolkit::index($files, "id");

    //     foreach ($data as $key => $value) {
    //         if ($key == "origin") {
    //             $file = $this->getFileService()->getFileObject($value["id"]);
    //             $file = $this->getFileService()->uploadFile("article", $file);
    //             $data[$key]["file"] = $file;

    //             $this->getFileService()->deleteFileByUri($files[$value["id"]]["uri"]);
    //         } else {
    //             $data[$key]["file"] = $files[$value["id"]];
    //         }
    //     }

    //     return $data;
    // }

    public function exploreAction(Request $request, $category)
    {
        $user = $this->getCurrentUser();
        $categoryArray = array();
        $province = $request->query->get('province');
        $orderBy = $request->query->get('orderBy');
        $conditions = array();

        $conditions['code'] = $category;
        if (!empty($conditions['code'])) {
            $categoryArray = $this->getSubSchoolCategoryService()->getCategoryByCode($conditions['code']);
            $childrenIds = $this->getSubSchoolCategoryService()->findCategoryChildrenIds($categoryArray['id']);
            $categoryIds = array_merge($childrenIds, array($categoryArray['id']));
            $conditions['categoryIds'] = $categoryIds;
        }
        unset($conditions['code']);

        if (!empty($province)) {
            $conditions['province'] = $province;
        }

        $conditions['status'] = 'normal';

        $subSchoolCount = $this->getSubSchoolService()->countSubSchools($conditions);
        $paginator = new Paginator($request, $subSchoolCount, 8);

        if (isset($orderBy) && $orderBy == 'latest') {
            $searchOrderBy = array('createdTime' => 'DESC');
            $subSchools = $this->getSubSchoolService()->searchSubSchools(
                $conditions,
                $searchOrderBy,
                $paginator->getOffsetCount(),
                $paginator->getPerPageCount()
            );
        } else {
            $searchOrderBy = array('recommendedSeq' => 'ASC');
            $conditions['recommended'] = 1;
            $recommendCount = $this->getSubSchoolService()->countSubSchools($conditions);
            $currentPage = $request->query->get('page') ? $request->query->get('page') : 1;
            $recommendPage = intval($recommendCount / 8);
            $recommendLeft = $recommendCount % 8;
            if ($currentPage <= $recommendPage) {
                $subSchools = $this->getSubSchoolService()->searchSubSchools(
                    $conditions,
                    $searchOrderBy,
                    ($currentPage - 1) * 8,
                    8
                );
            } elseif (($recommendPage + 1) == $currentPage) {
                $subSchools = $this->getSubSchoolService()->searchSubSchools(
                    $conditions,
                    $searchOrderBy,
                    ($currentPage - 1) * 8,
                    8
                );
                $conditions['recommended'] = 0;
                $branchSchoolsTemp = $this->getSubSchoolService()->searchSubSchools(
                    $conditions,
                    array('createdTime' => 'DESC'),
                    0,
                    8 - $recommendLeft
                );
                $subSchools = array_merge($subSchools, $branchSchoolsTemp);
            } else {
                $conditions['recommended'] = 0;
                $subSchools = $this->getSubSchoolService()->searchSubSchools(
                    $conditions,
                    array('createdTime' =>  'DESC'),
                    (8 - $recommendLeft) + ($currentPage - $recommendPage - 2) * 8,
                    8
                );
            }
        }

        $hostName = SubSchoolDomainHelper::getHostName($request);
        return $this->render('SubSchoolPlugin:Explore:explore.html.twig', array(
            'category' => $category,
            'subSchools' => $subSchools,
            'province' => $province,
            'paginator' => $paginator,
            'orderBy' => $orderBy,
            'hostName' => $hostName
        ));
    }

    public function treeNavAction(Request $request, $category, $path, $orderBy)
    {
        list($rootCategories, $categories, $activeIds) = $this->getSubSchoolCategoryService()->makeNavCategories($category);

        return $this->render("SubSchoolBundle:Explore:explore-nav.html.twig", array(
            'rootCategories' => $rootCategories,
            'categories'     => $categories,
            'category'       => $category,
            'activeIds'      => $activeIds,
            'path'           => $path,
            'orderBy'        => $orderBy
        ));
    }

    public function recommendAction(Request $request, $id)
    {
        $subSchool = $this->getSubSchoolService()->getSubSchool($id);

        if ($request->getMethod() == 'POST') {

            $number = $request->request->get('number');
            $fields = array(
                'recommended' => 1,
                'recommendedSeq' => (int)$number,
                'recommendedTime' => time(),
            );
            $subSchool = $this->getSubSchoolService()->updateSubSchool($id, $fields);
            $this->getLogService()->info('sub_school', 'recommend_sub_school', "管理员推荐分校({$subSchool['name']}#{$subSchool['id']})", $subSchool);

            return $this->redirect($this->generateUrl('admin_sub_school'));
        }

        return $this->render('SubSchoolBundle:SubSchoolManage:sub-school-recommend-modal.html.twig', array(
            'subSchool' => $subSchool,
        ));
    }

    public function recommendCancelAction(Request $request, $id)
    {
        $subSchool = $this->getSubSchoolService()->updateSubSchool($id, array('recommended' => 0));
        $this->getLogService()->info('sub_school', 'cancel_recommend_sub_school', "管理员取消推荐分校({$subSchool['name']}#{$subSchool['id']})", $subSchool);

        if ($subSchool['recommended'] == 0) {
            $response = array('success' => true, 'message' => '');
        } else {
            $response = array('failed' => true, 'message' => '');
        }
        return $this->createJsonResponse($response);
    }

    public function approvalAction(Request $request)
    {
        $conditions = array();

        if ($name = $request->query->get('name')) {
            $conditions['name'] = $name;
        }
        $conditions['status'] = 'auditing';
        $paginator = new Paginator(
            $request,
            $this->getBranchSchoolService()->searchBranchSchoolCount($conditions),
            20
        );
        $approvalLists = $this->getBranchSchoolService()->searchBranchSchools($conditions,
            array('createdTime', 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );
        $userIds = ArrayToolkit::column($approvalLists, 'applicantId');
        $applicants = $this->getUserService()->findUsersByIds($userIds);
        $applicants = ArrayToolkit::index($applicants, 'id');
        return $this->render('BranchSchoolBundle:BranchSchoolAdmin:sub-school-approval.html.twig', array(
            'approvalLists' => $approvalLists,
            'paginator' => $paginator,
            'applicants' => $applicants,
        ));
    }

    public function notificationAction(Request $request)
    {
        $status = $request->query->get('status');
        if ($status == 'false') {
            $user = $this->getCurrentUser();
            $subSchool = $this->getSubSchoolService()->findSubSchoolByOrgId($user['orgId']);
            $hostName = SubDomainHelper::getHostName($request);
        }

        return $this->render('SubSchoolBundle:SubSchoolManage:notification.html.twig', array(
            'status' => $status,
            'subSchool' => isset($subSchool) ? $subSchool : '',
            'hostName' => isset($hostName) ? $hostName : '',
        ));
    }

    public function aboutAction($orgId, $config)
    {
        $school = $this->getSubSchoolService()->findSubSchoolByOrgId($orgId);

        $realWidth = 0;
        $realHeight = 0;

        if (!empty($school)) {
            $length = strlen($school['logo']);
            $logo = substr($school['logo'], 6, $length - 12);
            $filePath = 'public:/' . $logo;
            $fullPath = $this->getFileService()->parseFileUri($filePath);
            $size = getimagesize($fullPath['fullpath']);
            $ratioHeight = $size[1] / 190;
            $ratioWidth = $size[0] / 285;
            $ratio = $ratioWidth > $ratioHeight ? $ratioWidth : $ratioHeight;
            if ($ratio > 1) {
                $realHeight = $size[1] / $ratio;
                $realWidth = $size[0] / $ratio;
            } else {
                $realHeight = 190;
                $realWidth = 285;
            }
        }

        return $this->render('SubSchoolBundle:Default:about.html.twig', array(
            'school' => $school,
            'realHeight' => $realHeight,
            'realWidth' => $realWidth,
            'config' => $config
        ));
    }

    public function removePictureAction($id, $type)
    {
        $subSchool = $this->getSubSchoolService()->getSubSchool($id);
        if ($type == 'logo'){
            $site = array(
                'name'      =>  $subSchool['name'],
                'slogan'    =>  '',
                'logo'      =>  ''
            );
            $this->getSettingService()->setByNamespace('org-'.$subSchool['orgId'], 'site', $site);
            $this->getSubSchoolService()->updateSubSchool($subSchool['id'], array('logo' => ''));
        }
        $this->getLogService()->info('system', 'update_settings', "移除分校 (#{$subSchool['name']}) LOGO");
        return $this->createJsonResponse(true);
    }

    protected function getSubSchoolService()
    {
        return $this->createService('SubSchoolPlugin:SubSchool:SubSchoolService');
    }

    /**
     * @return SubSchoolCategoryServiceImpl
     */

    protected function getSubSchoolCategoryService()
    {
        return $this->createService('SubSchoolPlugin:SubSchool:CategoryService');
    }


    protected function getOrgService()
    {
        return $this->createService('Org:OrgService');
    }


    protected function getUserService()
    {
        return $this->createService('User:UserService');
    }

    protected function getArticleService()
    {
        return $this->createService('Article:ArticleService');
    }

    protected function getFileService()
    {
        return $this->createService('Content:FileService');
    }

    protected function getLogService()
    {
        return $this->createService('System:LogService');
    }

    protected function getSettingService()
    {
        return $this->createService('System:SettingService');
    }


}
