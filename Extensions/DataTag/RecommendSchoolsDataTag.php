<?php

namespace SubSchool\SubSchoolBundle\Extensions\DataTag;

use Topxia\Common\ArrayToolkit;
use Topxia\WebBundle\Extensions\DataTag\DataTag;
use Topxia\WebBundle\Extensions\DataTag\CourseBaseDataTag;

class RecommendSchoolsDataTag extends CourseBaseDataTag implements DataTag
{
    /**
     * 获取推荐机构列表
     *
     * 可传入的参数：
     *   count    必需 机构数量，取值不能超过100
     *
     * @param  array $arguments           参数
     * @return array 推荐机构列表
     */
    public function getData(array $arguments)
    {
        $this->checkCount($arguments);
        $user          = $this->getCurrentUser();
        
        $conditions        = array('userIds' => array($user['id']));
        $subSchoolUsers = $this->getUserService()->searchUsers($conditions, array('id', 'DESC'), 0, 100);
        $schoolIds         = ArrayToolkit::column($subSchoolUsers, 'schoolId');
        $conditions['recommended'] = 1;
        $conditions['status'] = "normal";

        $subSchools = $this->getSubSchoolService()->searchSubSchools(
            $conditions,
            array('recommendedSeq', 'ASC',
                'recommendedTime', 'DESC'),
            0,
            $arguments['count']
        );

        $conditions         = array('userId' => $user['id']);
    
        $subSchoolAudits = $this->getSubSchoolService()->searchAuditRecords(
            $conditions,
            array('schoolId', 'DESC'),
            0, 100
        );

        foreach ($subSchools as &$subSchool) {
            if (in_array($subSchool['id'], $schoolIds)) {
                $subSchool['join'] = '1';
            } else {
                $subSchool['join'] = '0';
            }

            $subSchool['status'] = 'unapprove';

            foreach ($subSchoolAudits as $subSchoolAudit) {
                if (($subSchool['id'] == $subSchoolAudit['schoolId'])) {
                    $subSchool['status'] = $subSchoolAudit['status'];
                    break;
                }
            }
        }

        foreach ($subSchools as $key => $resultItem) {
            $subSchools[$key]['site'] = 'http://'.$resultItem['site'].$this->getServiceKernel()->getParameter('custom_domain');
        }

        return $subSchools;
    }

    protected function getSubSchoolService()
    {
        return $this->getServiceKernel()->createService('SubSchool:SubSchool.SubSchoolService');
    }
}
