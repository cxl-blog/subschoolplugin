<?php

namespace SubSchool\SubSchoolBundle\Extensions\DataTag;

use Topxia\Common\ArrayToolkit;
use Topxia\WebBundle\Extensions\DataTag\DataTag;
use Topxia\WebBundle\Extensions\DataTag\CourseBaseDataTag;

class RecommendTeachersDataTag extends CourseBaseDataTag implements DataTag
{
    /**
     * 获取推荐老师列表
     *
     * 可传入的参数：
     *   count    必需 老师数量，取值不能超过100
     *
     * @param  array $arguments           参数
     * @return array 推荐老师列表
     */
    public function getData(array $arguments)
    {
        $this->checkCount($arguments);
        $conditions = array(
            'roles'    => 'ROLE_TEACHER',
            'locked'   => 0,
            'orgCode'  => $arguments['org']
        );
        if ($arguments['org'] == '1.') {
            $conditions['promoted'] = 1;
            $orderBy = array(
                'promoted', 'DESC',
                'promotedSeq', 'ASC',
                'promotedTime', 'DESC',
                'createdTime', 'DESC'
            );
            $sortSeq = 'promotedSeq';
            $sortTime = 'promotedTime';
        } else {
            $conditions['schoolPromoted'] = 1;
            $orderBy = array(
                'schoolPromoted', 'DESC',
                'schoolPromotedSeq', 'ASC',
                'schoolPromotedTime', 'DESC',
                'createdTime', 'DESC'
            );
            $sortSeq = 'schoolPromotedSeq';
            $sortTime = 'schoolPromotedTime';
        }
        $users = $this->getUserService()->searchUsers($conditions, $orderBy, 0, $arguments['count']);

        $promotedSeq  = ArrayToolkit::column($users, $sortSeq);
        $promotedTime = ArrayToolkit::column($users, $sortTime);
        array_multisort($promotedSeq, SORT_ASC, $promotedTime, SORT_DESC, $users);

        $profiles = $this->getUserService()->findUserProfilesByIds(ArrayToolkit::column($users, 'id'));
        $user     = $this->getUserService()->getCurrentUser();

        if ($user->isLogin()) {
            $myfollowings = $this->getUserService()->filterFollowingIds($user['id'], ArrayToolkit::column($users, 'id'));
        } else {
            $myfollowings = array();
        }

        foreach ($users as $key => $user) {
            if ($user['id'] == $profiles[$user['id']]['id']) {
                $users[$key]['about']   = $profiles[$user['id']]['about'];
                $users[$key]['profile'] = $profiles[$user['id']];
            }

            $users[$key]['isFollowed'] = in_array($user['id'], $myfollowings) ? 1 : 0;
        }

        return $this->unsetUserPasswords($users);
    }
}
