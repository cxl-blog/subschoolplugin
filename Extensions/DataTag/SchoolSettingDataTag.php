<?php

namespace SubSchool\SubSchoolBundle\Extensions\DataTag;

use Topxia\WebBundle\Extensions\DataTag\DataTag;
use Topxia\WebBundle\Extensions\DataTag\BaseDataTag;

class SchoolSettingDataTag extends BaseDataTag implements DataTag
{
    public function getData(array $arguments)
    {
        $schoolSetting = $this->getSettingService()->get('school');

        return $schoolSetting;
    }

    protected function getSettingService()
    {
        return $this->getServiceKernel()->createService('System.SettingService');
    }
}