<?php

namespace SubSchoolPlugin\Listener;

use Topxia\Service\Common\ServiceKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use SubSchoolPlugin\Common\SubSchoolDomainHelper;

class KernelControllerListener
{
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        if ($event->getRequestType() == HttpKernelInterface::MASTER_REQUEST) {
            $request = $event->getRequest();
            $this->switchOrgByRequest($request);
            $currentOrgId = $this->isCurrentOrgId();

            if (SubSchoolDomainHelper::isAdminPath($request) && $currentOrgId) {
                $biz = $this->container->get('biz');

                $url = $this->getSwitchAdminUrl($request, $biz['user']->orgId);
                $event->setController(function () use ($url) {
                    return new RedirectResponse($url);
                });
            }
        }
    }

    protected function switchOrgByRequest($request)
    {
        $subName = SubSchoolDomainHelper::getSubName($request);
        $subSchool = $this->getSubSchoolService()->getSubSchoolBySite($subName);
        $orgId = (!empty($subSchool)) ? $subSchool['orgId'] : 1;
        $biz = $this->container->get('biz');
        if ($biz['user']->getSelectOrgId() != $orgId) {
            $this->getOrgService()->switchOrg($orgId);
        }
    }

    protected function isCurrentOrgId()
    {
        $biz = $this->container->get('biz');
        if ($biz['user']->getSelectOrgId() != $biz['user']->orgId) {
            return true;
        }
        return false;
    }

    protected function getSwitchAdminUrl($request, $orgId)
    {
        $subSchool = $this->getSubSchoolService()->getSubSchoolByOrgId($orgId);
        $site = empty($subSchool) ? '' : $subSchool['site'];
        $url = SubSchoolDomainHelper::buildAdminPath($request, $site);
        return $url;
    }


    protected function getOrgService()
    {
        return ServiceKernel::instance()->createService('Org:OrgService');
    }

    protected function getSubSchoolService()
    {
        return ServiceKernel::instance()->createService('SubSchoolPlugin:SubSchool:SubSchoolService');
    }
}
