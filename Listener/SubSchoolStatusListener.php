<?php

namespace SubSchoolPlugin\Listener;

use Topxia\Service\Common\ServiceKernel;
use AppBundle\Common\Exception\AccessDeniedException;
use SubSchoolPlugin\Common\SubSchoolDomainHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class SubSchoolStatusListener
{
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->getRequestType() == HttpKernelInterface::MASTER_REQUEST) {
            $request = $event->getRequest();
            $subName = SubSchoolDomainHelper::getSubName($request);
            $isMainSite = SubSchoolDomainHelper::isMainSite($request);
            if (!$isMainSite) {
                $subSchool = $this->getSubSchoolService()->getSubSchoolBySite($subName);

                if ($subSchool) {
                    if (!$subSchool['status']== 'disable') {
                        throw new AccessDeniedException('该分校未开启', null, 0);
                    }
                } else {
                    $url = SubSchoolDomainHelper::buildMainSiteUrl($request);
                    $response = new RedirectResponse($url, '302');
                    $event->setResponse($response);
                }
            }

        }
    }

    private function getUserService()
    {
        return ServiceKernel::instance()->createService('User.UserService');
    }

    protected function getOrgService()
    {
        return ServiceKernel::instance()->createService('Org:OrgService');
    }

    protected function getSubSchoolService()
    {
        return ServiceKernel::instance()->createService('SubSchoolPlugin:SubSchool:SubSchoolService');
    }
}