<?php

namespace SubSchool;

class PluginSystem
{
    const VERSION = '1.0.0';
    const RELEASE_NOTES = 'http://open.edusoho.com/app/subschool';
}
