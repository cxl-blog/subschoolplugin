define(function (require, exports, module) {
    "use strict";

    var Validator = require('bootstrap.validator');
    var Notify = require('common/bootstrap-notify');
    require('common/validator-rules').inject(Validator);

    exports.run = function() {
        var $form = $('#category-form');
        var $saveBtn = $('#category-save-btn');
        preventRepeatSubmit();
        var validator = new Validator({
            element: $form,
            autoSubmit: false,
            onFormValidated: function(error, results, $form) {
                if (error) {
                    $saveBtn.removeClass("disabled");
                    return false;
                }
                $saveBtn.button('submiting').addClass('disabled').attr("disabled","disabled");

                $.post($form.attr('action'), $form.serialize()).then(function(html){
                    Notify.success('保存分校分类成功！');
                    window.location.reload();
                }, function() {
                    Notify.danger("添加分校分类失败，请重试！");
                });
            }
        });

        validator.addItem({
            element: '#category-name-field',
            required: true,
            rule: 'maxlength{max:36} remote'
        });


        validator.addItem({
            element: '#category-code-field',
            required: true,
            rule: 'maxlength{max:36} remote'
        });

     $form.closest('.modal-dialog').on('click','.js-delete-category', function() {
            if (!confirm('真的要删除该分校分类吗？')) {
                return ;
             }
            $.get($(this).data('url'), function(response) {

                if (response.status == 'error') {
                    Notify.danger(response.message);
                } else {
                    window.location.reload();
                    Notify.success(response.message);
                }

            }, 'json').error(function(error) {
                Notify.danger("删除分校分类失败，请重试！");
            });

            return false;

        });

          function preventRepeatSubmit (){
                       $saveBtn.on('click',function(){
                                if($(this).hasClass('disabled')){
                                        return;
                                }
                                $(this).addClass('disabled');
                       })
          }

    };
});