define(function(require, exports, module) {

	var Notify = require('common/bootstrap-notify');

	exports.run = function() {

		$(".audit-join").on('click', function(){

			$.post($(this).data('url'), function(data){

					if(data.status == 1){
						Notify.success(data.info);
							window.location.href = data.data;
					}else{
						Notify.danger(data.info);
					}

			});

		});

	}
});
