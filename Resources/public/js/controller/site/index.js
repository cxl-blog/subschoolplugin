define(function(require, exports, module) {

    var AutoComplete = require('edusoho.autocomplete');
    var Validator = require('bootstrap.validator');
    require('common/validator-rules').inject(Validator);
    require('jquery.form');
    var Notify = require('common/bootstrap-notify');
    var WebUploader = require('edusoho.webuploader');


    exports.run = function() {
        
        var validator = new Validator({
            element: '#branch-school-setting-form'
        });

        var $form = $("#branch-school-setting-form");

        var uploader = new WebUploader({
            element: '#schoolLogo-upload'
        });

        uploader.on('uploadSuccess', function(file, response ) {
            var url = $("#schoolLogo-upload").data("gotoUrl");
                
            $.post(url, response ,function(data){
                $("#schoolLogo-container").html("<img class='img-responsive' src='"+data.url+"'>");
                $('#logo').val(data.url);
                $("#subSchool-logo-remove").show();
                Notify.success(Translator.trans('上传分校LOGO成功！'));
            });
        });

        $("#subSchool-logo-remove").on('click', function(){
            if (!confirm(Translator.trans('确认要删除吗？'))) return false;
            var $btn = $(this);
            $.post($btn.data('url'), function(){
                $("#schoolLogo-container").html('');
                $form.find('[name=logo]').val('');
                $btn.hide();
                Notify.success(Translator.trans('删除分校LOGO成功！'));
            }).error(function(){
                Notify.danger(Translator.trans('删除分校LOGO失败！'));
            });
        });
        
        Validator.addRule(
        'officePhone',
        /^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\d{8}$)/,
        '电话号码格式不正确'
        );

         $('[name="phone"]').on('change',function(){
            if(!(/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\d{8}$)/.test($('[name="phone"]').val()))){ 
                validator.addItem({
                    element: '[name="phone"]',
                    rule:'phone'
                });  
                return false; 
            }else{
                 validator.addItem({
                    element: '[name="phone"]',
                    rule:'officePhone'
                });  
                return false; 
            }
        });

        validator.addItem({
            element: '[name="email"]',
            rule: 'email'
        });

    };

});