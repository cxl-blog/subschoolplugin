define(function(require, exports, module) {

    var AutoComplete = require('edusoho.autocomplete');
    var Validator = require('bootstrap.validator');
    require('common/validator-rules').inject(Validator);
    require('jquery.form');
    var Notify = require('common/bootstrap-notify');
    var WebUploader = require('edusoho.webuploader');


    exports.run = function() {
        
        var validator = new Validator({
            element: '#branch-school-create-form',
            autoSubmit: false,
            onFormValidated: function(error, results, $form) {
                if (error) {
                    return false;
                }

                $('#branch-school-create-btn').button('submiting').addClass('disabled');
                var targetUrl = $form.data('url');
                $.post($form.attr('action'), $form.serialize(), function(html) {
                    if(html.info){
                        Notify.danger(html.info);
                        return false;
                    }
                    Notify.success('提交申请成功！');
                    window.location.href = targetUrl;
                }).error(function(){
                    Notify.danger('提交申请失败！');
                });

            }
        });

        var $form = $("#branch-school-create-form");


        var uploader = new WebUploader({
            element: '#schoolLogo-upload'
        });

        uploader.on('uploadSuccess', function(file, response ) {
            var url = $("#schoolLogo-upload").data("gotoUrl");

            $.post(url, response ,function(data){
                $("#schoolLogo-container").html("<img class='img-responsive' src='"+data.url+"'>");
                $('#logo').val(data.url);
                $("#subSchool-logo-remove").show();
                Notify.success(Translator.trans('上传分校LOGO成功！'));
            });
        });

        var uploader = new WebUploader({
            element: '#businessLicense-upload'
        });

        uploader.on('uploadSuccess', function(file, response ) {
            var url = $("#businessLicense-upload").data("gotoUrl");

            $.post(url, response ,function(data){
                $("#businessLicense-container").html("<img class='img-responsive' src='"+data.url+"'>");
                $('#businessLicense').val(data.url);
                Notify.success(Translator.trans('上传分校营业执照成功！'));
            });
        });

        $("#subSchool-logo-remove").on('click', function(){
            if (!confirm(Translator.trans('确认要删除吗？'))) return false;
            var $btn = $(this);
            $.post($btn.data('url'), function(){
                $("#schoolLogo-container").html('');
                $form.find('[name=logo]').val('');
                $btn.hide();
                Notify.success(Translator.trans('删除分校LOGO成功！'));
            }).error(function(){
                Notify.danger(Translator.trans('删除分校LOGO失败！'));
            });
        });

        Validator.addRule(
        'noNumberFirst',
        /^[a-zA-Z]+[a-zA-Z0-9]+?$/,
        'URL路径只能包含字母和数字,请以字母开头!长度不少于2位'
        );

        Validator.addRule(
        'integernumber',
        /^(?:0|[1-9][0-9]?|100)$/,
        '分成比例只能输入0~100之间的整数。'
        );

        Validator.addRule(
        'newnumber',
        /^\+?[1-9][0-9]*$/,
        '请输入正确的银行账号!。'
        );

        Validator.addRule(
        'officePhone',
        /^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\d{8}$)/,
        '电话号码格式不正确'
        );

        validator.addItem({
            element: '[name="schoolname"]',
            required: true,
            rule: 'remote'
        });       

        validator.addItem({
            element: '[name="branchPrincipal"]',
            required: true
        });

        validator.addItem({
            element: '[name="address"]',
            required: true
        });

        $('[name="principalPhone"]').on('change',function(){
            if(!(/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\d{8}$)/.test($('[name="principalPhone"]').val()))){ 
                validator.addItem({
                    element: '[name="principalPhone"]',
                    required: true,
                    rule:'phone'
                });  
                return false; 
            }else{
                 validator.addItem({
                    element: '[name="principalPhone"]',
                    required: true,
                    rule:'officePhone'
                });  
                return false; 
            }
        });
        validator.addItem({
            element: '[name="principalPhone"]',
            required: true
        });  
        

        validator.addItem({
            element: '[name="contact"]',
            required: true
        });

         $('[name="phone"]').on('change',function(){
            if(!(/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\d{8}$)/.test($('[name="phone"]').val()))){ 
                validator.addItem({
                    element: '[name="phone"]',
                    required: true,
                    rule:'phone'
                });  
                return false; 
            }else{
                 validator.addItem({
                    element: '[name="phone"]',
                    required: true,
                    rule:'officePhone'
                });  
                return false; 
            }
        });

         validator.addItem({
                element: '[name="phone"]',
                required: true
        });  

        validator.addItem({
            element: '[name="email"]',
            required: true,
            rule: 'email'
        });

        validator.addItem({
            element: '[name="subSchoolCategoryId"]',
            required: true,
            errormessageRequired: '请选择分类'
        });

        validator.addItem({
            element: '[name="proportion"]',
            required: true,
            rule: 'integernumber'
        });

        // validator.addItem({
        //     element: '[name="logo"]',
        //     required: true,
        //     errormessageRequired: '请上传LOGO图片'
        // });

        validator.addItem({
            element: '[name="cover"]',
            required: true,
            errormessageRequired: '请上传封面图片'
        });

        validator.addItem({
            element: '[name="businessLicense"]',
            required: true,
            errormessageRequired: '请上传营业执照'
        });

        validator.addItem({
            element: '[name="site"]',
            required: true,
            rule: 'noNumberFirst remote'
        });

        validator.addItem({
            element: '[name="accountNumber"]',
            rule: 'newnumber'
        });

    };

});