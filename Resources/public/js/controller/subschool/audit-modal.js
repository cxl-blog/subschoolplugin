define(function(require, exports, module) {

    var Notify = require('common/bootstrap-notify');

    exports.run = function() {
        $('#audit-pass-btn').click(function() {
            var url = $(this).data('url');
            var form = $('#school-audit-form');
            $('#school-audit-result').val('success');
            $.post(url, form.serialize(), function() {
                location.reload();
            });
        })

        $('#audit-reject-btn').click(function() {
            var url = $(this).data('url');
            var form = $('#school-audit-form');
            $('#school-audit-result').val('fail');
            var content = $.trim($('#audit-reject-text').val());
            if (content == '') {
                Notify.danger('请输入拒绝理由');
                return ;
            }
            $.post(url, form.serialize(), function() {
                location.reload()
            });
        })
    }
})