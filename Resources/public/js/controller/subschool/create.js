define(function(require, exports, module) {

    var AutoComplete = require('edusoho.autocomplete');
    var DynamicCollection = require('../widget/dynamic-collection4');
    var Validator = require('bootstrap.validator');
    require('common/validator-rules').inject(Validator);
    require('jquery.form');
    var Notify = require('common/bootstrap-notify');
    var WebUploader = require('edusoho.webuploader');


    exports.run = function() {

        var validator = new Validator({
            element: '#branch-school-create-form',
            autoSubmit: false,
            onFormValidated: function(error, results, $form) {
                if (error) {
                    return false;
                }

                $('#branch-school-create-btn').button('submiting').addClass('disabled');
                var targetUrl = $form.data('url');
                var type = $form.data('type');
                $.post($form.attr('action'), $form.serialize(), function(html) {
                    if(html.info){
                        Notify.danger(html.info);
                        return false;
                    }
                    if(type == 'create'){
                        Notify.success('分校添加成功');
                    }else{
                        Notify.success('分校更新成功');
                    }
                    window.location.href = targetUrl;
                }).error(function(){
                    if(type == 'create'){
                        Notify.danger('分校添加失败');
                    }else{
                        Notify.danger('分校更新失败');
                    }
                });

            }
        });

        var dynamicCollection = new DynamicCollection({
            element: '#admins-form-group',
            onlyAddItemWithModel: true,
            beforeDeleteItem: function(e){
                var adminCounts=$("#admin-list-group").children("li").length;
                if(adminCounts <= 1){
                    Notify.danger("分校至少需要一个管理员！");
                    return false;
                }
                return true;
            }
        });

        var autocomplete = new AutoComplete({
            trigger: '#admin-input',
            dataSource: $("#admin-input").data('url'),
            filter: {
                name: 'stringIgnoreCaseMatch',
                options: {
                    key: 'nickname'
                }
            },
            selectFirst: true
        }).render();

        autocomplete.on('itemSelect', function(data){
            var error = '';
            dynamicCollection.element.find('input[name="adminIds[]"]').each(function(i, item) {
                if (parseInt(data.adminId) == parseInt($(item).val())) {
                    error = '该管理员已添加，不能重复添加！';
                }
            });
            if (error) {
                Notify.danger(error);
                dynamicCollection.clearInput();
            } else {
                dynamicCollection.addItemWithModel(data);
            }
        });

        dynamicCollection.on('beforeAddItem', function(value) {
            autocomplete.set('inputValue', null);
            autocomplete.setInputValue(value);
        });

        $('#admin-add').on('click', function(){
            var inputValue = $('#admin-input').val();
            if(inputValue.length<1 || !autocomplete.items){
                    return ;
            }
            for(var i=0;i<autocomplete.items.length;i++){
                 if(inputValue == autocomplete.items[i].textContent){
                    autocomplete.set("selectedIndex", i);
            autocomplete.selectItem(i);
            return ;
                 }
            }
        });

        $('#branch-school-create-btn').on('click', function(){
            var element = dynamicCollection.element.find('input[name="adminIds[]"]');
            if(element.length == 0){
                Notify.danger("分校至少需要一个管理员！");
                return false;
            }

        });


        var $form = $("#branch-school-create-form");

        // var uploader = new WebUploader({
        //     element: '#license-upload'
        // });

        // uploader.on('uploadSuccess', function(file, response ) {
        //     var url = $("#license-upload").data("gotoUrl");
        //
        //     $.post(url, response ,function(data){
        //         $("#license-container").html("<img class='img-responsive' src='"+data.url+"'>");
        //         $('#license').val(data.url);
        //         Notify.success(Translator.trans('上传分校营业执照成功！'));
        //     });
        // });

        $("#subSchool-logo-remove").on('click', function(){
            if (!confirm(Translator.trans('确认要删除吗？'))) return false;
            var $btn = $(this);
            $.post($btn.data('url'), function(){
                $("#schoolLogo-container").html('');
                $form.find('[name=logo]').val('');
                $btn.hide();
                Notify.success(Translator.trans('删除分校LOGO成功！'));
            }).error(function(){
                Notify.danger(Translator.trans('删除分校LOGO失败！'));
            });
        });

        Validator.addRule(
            'noNumberFirst',
            /^[a-zA-Z]+[a-zA-Z0-9\.]$/,
            'URL路径只能包含字母和数字，请以字母开头。长度不少于2位，支持2级域名，'
        );

        // Validator.addRule(
        // 'officePhone',
        // /^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\d{8}$)/,
        // '电话号码格式不正确'
        // );

        validator.addItem({
            element: '[name="name"]',
            required: true,
            rule: 'remote'
        });

        // validator.addItem({
        //     element: '[name="director"]',
        //     required: false,
        //     rule: 'chinese_alphanumeric'
        // });

        // $('[name="directorPhone"]').on('change',function(){
        //     if(!(/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\d{8}$)/.test($('[name="directorPhone"]').val()))){
        //         validator.addItem({
        //             element: '[name="directorPhone"]',
        //             required: false,
        //             rule:'phone'
        //         });
        //         return false;
        //     }else{
        //          validator.addItem({
        //             element: '[name="directorPhone"]',
        //             required: false,
        //             rule:'officePhone'
        //         });
        //         return false;
        //     }
        // });
        // validator.addItem({
        //     element: '[name="directorPhone"]',
        //     required: false
        // });

        //  $('[name="phone"]').on('change',function(){
        //     if(!(/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$|(^(13[0-9]|15[0|3|6|7|8|9]|18[8|9])\d{8}$)/.test($('[name="phone"]').val()))){
        //         validator.addItem({
        //             element: '[name="phone"]',
        //             required: true,
        //             rule:'phone'
        //         });
        //         return false;
        //     }else{
        //          validator.addItem({
        //             element: '[name="phone"]',
        //             required: true,
        //             rule:'officePhone'
        //         });
        //         return false;
        //     }
        // });

        // validator.addItem({
        //     element: '[name="categoryId"]',
        //     required: true,
        //     errormessageRequired: '请选择分类'
        // });

        validator.addItem({
            element: '[name="cover"]',
            required: true,
            errormessageRequired: '请上传封面图片'
        });

        // validator.addItem({
        //     element: '[name="license"]',
        //     required: false,
        //     errormessageRequired: '请上传营业执照'
        // });

        validator.addItem({
            element: '[name="site"]',
            required: true,
            rule: 'noNumberFirst remote'
        });

    };

});
