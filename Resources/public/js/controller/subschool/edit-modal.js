define(function(require, exports, module) {

    var Notify = require('common/bootstrap-notify');
    var Validator = require('bootstrap.validator');
    var WebUploader = require('edusoho.webuploader');
    var Uploader = require('upload');
    require('jquery.bootstrap-datetimepicker');
    require('common/validator-rules').inject(Validator);
    require('jquery.form');
    Validator.addRule(
        'time_check',
    /^(?:(?!0000)[0-9]{4}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)-02-29) ([0-1]{1}[0-9]{1})|(2[0-4]{1}):[0-5]{1}[0-9]{1}$/,
    '请输入正确的日期和时间,格式如XXXX-MM-DD hh:mm'
    );

    Validator.addRule('check_org',function(opitions,commit){
        var orgId = opitions.element.val();     
        var result;
        $.ajax({
            url: opitions.element.data('url'),
            data: {'orgId':orgId},
            async:false,
            success :function(data){
                result = data;
            }
        })
         return result; 
                 
    },"该组织分校已被占用");

    Validator.addRule('check_site',function(opitions,commit){
        var site = opitions.element.val();
        var result;
        $.ajax({
            url: opitions.element.data('url'),
            data: {'site' :site},
            async:false,
            success :function(data){
                result = data;
            }
        })
        return result;
    },"该站点已经被占用");
    
    exports.run = function() {

        var $modal = $('#branch-school-edit-form').parents('.modal');

        $form = $('#branch-school-edit-form');

        var validator = new Validator({
            element: '#branch-school-edit-form',
            autoSubmit: false,
            onFormValidated: function(error, results, $form) {
                if (error) {
                    return false;
                }

                $('#branch-school-edit-btn').button('submiting').addClass('disabled');

                $.post($form.attr('action'), $form.serialize(), function(html) {
                    $modal.modal('hide');
                    window.location.reload();

                }).error(function(){
                    Notify.danger('操作失败');
                });
            }
        });

        validator.addItem({
            element: '[name="name"]',
            required: true
        });

        validator.addItem({
            element: '[name="address"]',
            required: true
        });

        validator.addItem({
            element: '[name="site"]',
            required: true,
            rule: 'check_site'
        });

        validator.addItem({
            element: '[name=startTime]',
            required: true,
            rule: 'time_check',
            display: '开始时间'
        });

        validator.addItem({
            element: '[name=endTime]',
            required: true,
            rule: 'time_check date_check',
            display: '结束时间'
        });
 
        var dateTimePicker = function(){
                var now = new Date();
                $("[name=startTime]").datetimepicker({
                    language: 'zh-CN',
                    autoclose: true
                }).on('hide', function(ev){
                    validator.query('[name=startTime]').execute();
                });

                $('[name=startTime]').datetimepicker('setStartDate', now);

                $('[name=startTime]').datetimepicker().on('changeDate',function(){
                    $('[name=endTime]').datetimepicker('setStartDate',$('[name=startTime]').val().substring(0,16));
                });

                $("[name=endTime]").datetimepicker({
                    language: 'zh-CN',
                    autoclose: true
                }).on('hide', function(ev){
                    validator.query('[name=endTime]').execute();
                });

                $('[name=endTime]').datetimepicker('setStartDate', now);

                $('[name=endTime]').datetimepicker().on('changeDate',function(){
                      $('[name=startTime]').datetimepicker('setEndDate',$('[name=endTime]').val().substring(0,16));
                });               
        }();


         var uploader = new WebUploader({
            element: '#branch-school-picture-upload'
        });

        uploader.on('uploadSuccess', function(file, response ) {
                $("#branch-school-picture-container").html('<img class="img-responsive" src="' + response.url + '">');
                $('#branch-couver-picture').val(response.uri);
                Notify.success('上传封面图片成功！');
        });

    };

});