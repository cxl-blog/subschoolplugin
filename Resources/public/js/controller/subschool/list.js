define(function(require, exports, module) {

  	var Notify = require('common/bootstrap-notify');

	exports.run = function() {
		var $table=$('#branch-school-table');

		$table.on('click','.open-branch-school,.close-branch-school',function(){
			var $trigger = $(this);
    		if (!confirm($trigger.attr('title') + '吗？')) {
    				return ;
    			}
    		$.post($(this).data('url'), function(html) {
                var tr = $(html);
                $table.find('#' + tr.attr('id')).replaceWith(html);
                Notify.success($trigger.attr('title') + '成功！');
            }).error(function(){
                Notify.danger($trigger.attr('title') + '失败');
            });
    	});

        //取消推荐操作
        $table.on('click','#recommended-cancel',function(){
            var url =  $(this).data('url');
            $.post(url,function(html){
                var tr = $(html);
                $table.find('#' + tr.attr('id')).replaceWith(html);
                Notify.success(Translator.trans('取消分校推荐操作成功!'));
            }).error(function(){
                Notify.danger(Translator.trans('取消分校推荐操作失败!'));
            });
        });

	}

});