define(function(require, exports, module) {
    require("jquery.jcrop-css");
    require("jquery.jcrop");
    var Notify = require('common/bootstrap-notify');
    var ImageCrop = require('edusoho.imagecrop');

    exports.run = function() {
        var $modal = $("#modal");

        //构建副本
        var imagecopy = $('#branch-pic-crop').clone();
        var imageCrop = new ImageCrop({
            element: "#branch-pic-crop",
            group: 'default',
            cropedWidth: 516,
            cropedHeight: 344
        });

        $('#branch-pic-crop').on('load', function(){
            imageCrop.get('img').destroy();
            var control = $('#modal .controls')[0];
            var $control = $(control);
            $control.prepend(imagecopy);
            var newimageCrop = new ImageCrop({
                element: "#branch-pic-crop",
                group: 'default',
                cropedWidth: 516,
                cropedHeight: 344
            });

              
        newimageCrop.on("afterCrop", function(response){
            var url = $("#upload-picture-crop-btn").data("gotoUrl");
            $.post(url, {images: response}, function(data){
                $modal.modal('hide');
                $("#branch-picture-container").show();
                $("#cover").val(data.large.file.url);
                $("#branch-picture-container").html("<img class='img-responsive' src='"+data.large.file.url+"'>")
            });

        });


        $("#upload-picture-crop-btn").click(function(e) {
            e.stopPropagation();

                newimageCrop.crop({
                    imgs: {
                        large: [516, 344],
                    }
                });
            });
        });

        
            
        imageCrop.on("afterCrop", function(response){
            var url = $("#upload-picture-crop-btn").data("gotoUrl");
            $.post(url, {images: response}, function(data){
                $modal.modal('hide');
                $("#branch-picture-container").show();
                $("#cover").val(data.large.file.url);
                $("#branch-picture-container").html("<img class='img-responsive' src='"+data.large.file.url+"'>")
            });

        });


        $("#upload-picture-crop-btn").click(function(e) {
            e.stopPropagation();

            imageCrop.crop({
                imgs: {
                    large: [516, 344],
                }
            });

        });
    };
});