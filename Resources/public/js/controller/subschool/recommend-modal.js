define(function(require, exports, module) {
	var Notify = require('common/bootstrap-notify');
	var Validator = require('bootstrap.validator');
    require('common/validator-rules').inject(Validator);

	exports.run = function(options) {
		var $modal = $('#course-recommend-form').parents('.modal');
        var $table=$('#branch-school-table');

		var validator = new Validator({
            element: '#course-recommend-form',
            autoSubmit: false,
            onFormValidated: function(error, results, $form) {
                if (error) {
                    return false;
                }
                $('#course-recommend-btn').button('submiting').addClass('disabled');
                $.post($form.attr('action'), $form.serialize(), function(html) {
                    var tr = $(html);
                    $modal.modal('hide');
                    $table.find('#' + tr.attr('id')).replaceWith(html);
                    Notify.success(Translator.trans('设置推荐分校操作成功!'));
                }).error(function(){
                    Notify.danger(Translator.trans('设置推荐分校操作失败!'));
                });
            }

        });

        validator.addItem({
            element: '[name="number"]',
            required: true,
            rule: 'integer min{min: 0} max{max: 10000}'
        });

	};

});

















