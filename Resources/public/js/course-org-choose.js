define(function(require, exports, module) {
     var OrgChoose = require('./org-choose');
         exports.run = function() {
                var $form = $("#form-course-copy");
                var $copyOrgCourse  = $("#copy-org-course");
                var $listLeft =$copyOrgCourse.find(".list.left");
                orgChoose  = new OrgChoose({
                    element: '#copy-org-course'
                });
                orgChoose.set("callback",function($li,moveType){
                         var orgId = $li.data("orgId");
                        if(moveType == 'right'){
                               $li.find("i").addClass('glyphicon-expand');
                               var $input_org = $("<input id='input_org_"+orgId+"' name='orgIds[]' type='hidden' value='"+orgId+"'>");
                               $form.append($input_org);
                        }
                        else if(moveType == 'left'){
                               $li.find("i").removeClass('glyphicon-expand');
                                $("#input_org_"+orgId).remove();
                        }
                });

              var $resultTip = $copyOrgCourse.find(".no-result");
//搜索
              $("#copy-org-course .search").on("keyup",function(){
                     var text = $(this).val();
                     if(text == ''){
                          $listLeft.find("li").removeClass("hide");
                          $resultTip.addClass("hide");
                          return;
                     }
                     var result = 0;
                     $listLeft.find("li").each(function(){
                             if($(this).text().indexOf(text) == -1 ){
                                   $(this).addClass('hide'); 
                             }
                             else{
                                $(this).removeClass('hide'); 
                                result =1;
                             }
                      })   
                    if( result == 0){
                         $resultTip.removeClass("hide");
                    }
                })



         }

});