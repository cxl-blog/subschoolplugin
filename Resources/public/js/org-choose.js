define(function(require, exports, module) {
     var Widget = require('widget');
     var courseChooseList= Widget.extend({
            attrs:{
                callback:function($li,moveType){}
            },
           events: {
                'click .list li' : 'chooseClick'
            },
            chooseClick:function(e){
                var $li = $(e.currentTarget);
                var moveType =  $li.closest(".list").hasClass("left") ? 'right' : 'left';
                var toAppenBox = this.element.find(".list." + moveType);
                $li.prependTo(toAppenBox); 
                  this.get("callback")($li,moveType);
            }
     });

     module.exports = courseChooseList;

});