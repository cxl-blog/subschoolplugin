import notify from 'common/notify';

var categoryLis = $('.category-li');
for (var i = 0; i< categoryLis.length; i++) {
    $(categoryLis[i]).show();
    $(categoryLis[i]).addClass('show');
    var height = $('.category-nav').height();
    if (height > 60) {
        $(categoryLis[i]).hide();
        $(categoryLis[i]).removeClass('show');
        break ;
    }
}
if (i < categoryLis.length) {
    $('.more-category').show();
}

var provinceLis = $('.province-li');
for (var i = 0; i< 11; i++) {
    $(provinceLis[i]).show();
    $(provinceLis[i]).addClass('show');
}

$('.tab-body ul').each(function(){
    if ($(this).find('li.active').length == 0) {
        $(this).find('li.all').addClass('active');
    }
});

$('.more-province').click(function() {
    var lis = $('.province-li').not($(".province-li.show"));
    lis.slideToggle();
    $('.more-province-show').toggle();
    $('.more-province-pack').toggle();
});

$('.more-category').click(function() {
    var lis = $('.category-li').not($(".category-li.show"));
    lis.slideToggle();
    $('.more-category-show').toggle();
    $('.more-category-pack').toggle();
})