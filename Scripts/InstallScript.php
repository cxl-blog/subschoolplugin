<?php

include_once __DIR__.'/BaseInstallScript.php';

class InstallScript extends BaseInstallScript
{

    public function install()
    {
        $connection = $this->getConnection();
        $this->createCategoryTable($connection);
        $this->createSubSchoolTable($connection);
        // $this->addDefaultCategory($connection);
        $this->alterOrdersTable($connection);
        $this->alterUserTable($connection);
        $this->createSubSchoolUserTable($connection);
        $this->addSubSchoolRole($connection);
        $this->updateSubSchoolRole($connection);
        $this->makeSubschoolCategoryOptional($connection);
        $this->openOrgEnable();
    }

    public function openOrgEnable()
    {
        $setting = $this->createService('System:SettingService')->get('magic', array());
        if ($setting['enable_org'] != 1) {
            $setting['enable_org'] = 1;
            $this->createService('System:SettingService')->set('magic', $setting);
        }
    }

    private function createCategoryTable($connection)
    {
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `sub_school_category` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `parentId` int(10) unsigned NOT NULL DEFAULT '0',
            `name` varchar(36) NOT NULL,
            `createdTime` int(10) unsigned NOT NULL DEFAULT '0',
            `updatedTime` int(10) unsigned NOT NULL DEFAULT '0',
            `code` varchar(36) NOT NULL,
            `seq` int(10) unsigned NOT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }

    private function createSubSchoolTable($connection)
    {
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `sub_school` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `name` varchar(128) NOT NULL COMMENT '分校名称',
              `adminIds` text COMMENT '管理员ID',
              `site` varchar(128) DEFAULT NULL COMMENT '域名',
              `status` enum('normal','disable','delete') NOT NULL DEFAULT 'normal' COMMENT '状态',
              `createdTime` int(10) unsigned NOT NULL,
              `updatedTime` int(10) unsigned NOT NULL,
              `logo` varchar(255) DEFAULT NULL COMMENT '徽标',
              `cover` varchar(255) DEFAULT NULL COMMENT '封面',
              `categoryId` int(10) NOT NULL COMMENT '分校分类id',
              `recommended` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为推荐分校',
              `recommendedSeq` int(10) unsigned NOT NULL DEFAULT '0',
              `recommendedTime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '推荐时间',
              `director` varchar(255) DEFAULT NULL COMMENT '分校负责人',
              `directorPhone` varchar(50) DEFAULT NULL COMMENT '负责人电话',
              `license` varchar(255) DEFAULT NULL COMMENT '营业执照',
              `studentNum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '学生人数',
              `lessonNum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '课程数量',
              `orgCode` varchar(255) NOT NULL DEFAULT '1.' COMMENT '组织分校内部编码',
              `orgId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '组织机构内部编码',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
            ");
    }

    private function makeSubschoolCategoryOptional($connection)
    {
        $connection->exec("
            ALTER TABLE `sub_school` MODIFY `categoryId` int(10) unsigned DEFAULT NULL;
            ");
    }

    // private function addDefaultCategory($connection)
    // {
    //     if (!$this->isRowExist('sub_school_category', 'id', 1)) {
    //         $connection->exec("
    //           INSERT INTO `sub_school_category` (`id`, `parentId`, `name`, `createdTime`, `updatedTime`, `code`, `seq`)
    //           VALUES (1, 0, '默认分类', 0, 0, 'default', 0);
    //       ");
    //     }
    // }

    private function alterOrdersTable($connection)
    {
        if (!$this->isFieldExist('orders', 'orgCode')) {
            $connection->exec("
                ALTER TABLE `orders` ADD `orgCode` varchar(255) NOT NULL DEFAULT '1.' COMMENT '组织机构内部编码';
            ");
        }

        if (!$this->isFieldExist('orders', 'orgId')) {
            $connection->exec("
                ALTER TABLE `orders` ADD `orgId` int(10) unsigned NOT NULL DEFAULT '0';
            ");
        }

        if (!$this->isFieldExist('orders', 'teacherId')) {
            $connection->exec("
                ALTER TABLE `orders` ADD `teacherId` int(10) unsigned NOT NULL DEFAULT '0';
            ");
        }

        if ($this->isFieldExist('orders', 'status')) {
            $connection->exec("
                    ALTER TABLE `orders` MODIFY `status` enum('created','paid','refunding','refunded','cancelled','settled') NOT NULL
                ");
        }
    }

    private function alterUserTable($connection)
    {
        if (!$this->isFieldExist('user', 'schoolPromoted')) {
            $connection->exec("
                ALTER TABLE `user` ADD `schoolPromoted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为分校推荐老师';
                ALTER TABLE `user` ADD `schoolPromotedSeq` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分校推荐老师顺序';
                ALTER TABLE `user` ADD `schoolPromotedTime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分校推荐老师时间';
            ");
        }
    }

    private function createSubSchoolUserTable($connection)
    {
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `sub_school_user` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `userId` int(10) unsigned NOT NULL DEFAULT '0',
            `schoolId` int(10) unsigned NOT NULL DEFAULT '0',
            `createdTime` int(10) unsigned NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }

    private function addSubSchoolRole($connection)
    {
        if (!$this->isRowExist('role', 'code', 'ROLE_SCHOOL_ADMIN')) {
            $connection->exec("
                INSERT INTO `role` (`id`, `name`, `code`, `data`, `createdTime`, `createdUserId`, `updatedTime`) VALUES (NULL, '分校管理员', 'ROLE_SCHOOL_ADMIN', '[\"admin\",\"admin_sub_school\",\"admin_school_setting\",\"admin_school_setting_manage\",\"admin_school_nav_manage\",\"admin_school_block\",\"admin_school_block_manage\",\"admin_school_order\",\"admin_school_order_manage\",\"admin_school_audit\",\"admin_school_audit_manage\",\"admin_school_audit_record\",\"admin_school_audit_record_manage\"]', '".time()."', '1', '0');
            ");
        }
    }

    private function updateSubSchoolRole($connection)
    {
        if ($this->isRowExist('role', 'code', 'ROLE_SCHOOL_ADMIN')) {
            $connection->exec("
                UPDATE `role` SET data = '[\"admin\",\"admin_user\",\"admin_user_show\",\"admin_user_manage\",\"admin_user_create\",\"admin_user_edit\",\"admin_user_roles\",\"admin_user_avatar\",\"admin_user_change_password\",\"admin_user_send_passwordreset_email\",\"admin_user_send_emailverify_email\",\"admin_user_lock\",\"admin_user_unlock\",\"admin_teacher\",\"admin_approval_manage\",\"admin_course\",\"admin_course_show\",\"admin_course_manage\",\"admin_course_content_manage\",\"admin_course_add\",\"admin_course_set_recommend\",\"admin_course_set_cancel_recommend\",\"admin_course_guest_member_preview\",\"admin_course_set_close\",\"admin_course_sms_prepare\",\"admin_course_set_clone\",\"admin_course_set_publish\",\"admin_course_set_delete\",\"admin_course_set_recommend_list\",\"admin_course_set_data\",\"admin_classroom\",\"admin_classroom_manage\",\"admin_classroom_content_manage\",\"admin_classroom_create\",\"admin_classroom_cancel_recommend\",\"admin_classroom_set_recommend\",\"admin_classroom_close\",\"admin_sms_prepare\",\"admin_classroom_open\",\"admin_classroom_delete\",\"admin_classroom_recommend\",\"admin_live_course\",\"admin_course_category\",\"admin_course_tag\",\"admin_operation\",\"admin_operation_article\",\"admin_announcement\",\"admin_block_manage\",\"admin_system\",\"admin_setting\",\"admin_setting_message\",\"admin_top_navigation\"]' WHERE code = 'ROLE_SCHOOL_ADMIN';
            ");
        }
    }


    protected function isFieldExist($table, $filedName)
    {
        $sql = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->getConnection()->fetchAssoc($sql);

        return empty($result) ? false : true;

    }

    protected function isRowExist($table, $column, $value)
    {
        $sql = "SELECT * FROM `{$table}` WHERE {$column} = ?";
        $result = $this->getConnection()->fetchAssoc($sql, array($value));
        return empty($result) ? false:true;
    }

}
