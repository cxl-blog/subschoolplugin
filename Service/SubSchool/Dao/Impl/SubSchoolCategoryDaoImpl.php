<?php

namespace SubSchool\Service\SubSchool\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use SubSchool\Service\SubSchool\Dao\SubSchoolCategoryDao;

class SubSchoolCategoryDaoImpl extends BaseDao implements SubSchoolCategoryDao
{
    protected $table = 'sub_school_category';

    public function createCategory($category)
    {
        $category['createdTime'] = time();
        $category['updatedTime'] = time();
        $affected = $this->getConnection()->insert($this->getTable(), $category);

        if ($affected <= 0) {
            throw $this->createDaoException("Insert {$this->getTable()} error.");
        }

        return $this->getCategory($this->getConnection()->lastInsertId());
    }

    public function getCategoriesByParentId($id)
    {
        $sql = "SELECT * FROM {$this->getTable()} WHERE parentId = ? ";

        return $this->getConnection()->fetchAll($sql, array($id)) ?: null;
    }

    public function updateCategory($id, $fields)
    {
        $fields['updatedTime'] = time();
        $this->getConnection()->update($this->getTable(), $fields, array('id' => $id));
        return $this->getCategory($id);
    }

    public function deleteCategory($id)
    {
        return $this->getConnection()->delete($this->getTable(), array('id' => $id));
    }

    public function searchCategoryCount($conditions)
    {
        $builder = $this->_createSearchQueryBuilder($conditions)
            ->select('COUNT(*)') ;

        return $builder->execute()->fetchColumn(0);
    }

    public function searchCategories($conditions, $orderBys, $start, $limit)
    {
        $this->filterStartLimit($start, $limit);
        $builder = $this->_createSearchQueryBuilder($conditions)
            ->select('*')
            ->setFirstResult($start)
            ->setMaxResults($limit);

        foreach ($orderBys as $orderBy) {
            $builder->addOrderBy($orderBy[0], $orderBy[1]);
        }

        return $builder->execute()->fetchAll() ?: array();
    }

    public function getCategory($id)
    {
        $sql = "SELECT * FROM {$this->getTable()} WHERE id = ? LIMIT 1";

        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    public function findAllCategories()
    {
        $sql = "SELECT * FROM {$this->getTable()} ORDER BY `seq` ASC ;";

        return $this->getConnection()->fetchAll($sql);
    }

    public function findCategoriesByIds(array $ids)
    {
        if(empty($ids)){
            return array();
        }

        $marks = str_repeat('?,', count($ids) - 1).'?';

        $that = $this;
        $keys = implode(',', $ids);
        return $this->fetchCached("ids:{$keys}", $marks, $ids, function ($marks, $ids) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE id IN ({$marks}) ;";

            return $that->getConnection()->fetchAll($sql, $ids);
        }

        );
    }

    public function findCategoryByCode($code)
    {
        $that = $this;

        return $this->fetchCached("code:{$code}", $code, function ($code) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE code = ? LIMIT 1";
            return $that->getConnection()->fetchAssoc($sql, array($code));
        }

        );
    }

    public function findRootCategories()
    {
        $sql = "SELECT * FROM {$this->table} WHERE parentId = ? ORDER BY `seq` ASC ;";

        return $this->getConnection()->fetchAll($sql, array(0));
    }

    protected function _createSearchQueryBuilder($conditions)
    {
        $builder = $this->createDynamicQueryBuilder($conditions)
            ->from($this->getTable())
            ->andWhere('code = :code')
            ->andWhere('name = :name')
            ->andWhere('parentId = :parentId')
            ->andWhere('id IN ( :categoryIds )')
        ;

        return $builder;
    }

    public function findCategoryByName($name)
    {
        $that = $this;

        return $this->fetchCached("name:{$name}", $name, function ($name) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE code = ? LIMIT 1";
            return $that->getConnection()->fetchAssoc($sql, array($name));
        }
        );
    }
}
