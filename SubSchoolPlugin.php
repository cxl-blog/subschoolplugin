<?php

namespace SubSchoolPlugin;

use Codeages\PluginBundle\System\PluginBase;
use SubSchoolPlugin\Biz\SubSchoolServiceProvider;

class SubSchoolPlugin extends PluginBase
{
    public function boot()
    {
        parent::boot();
        $biz = $this->container->get('biz');
        $biz->register(new SubSchoolServiceProvider());
    }
}
