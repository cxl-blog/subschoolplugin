<?php

namespace SubSchool\Service\SubSchool\Tests;

use Topxia\Service\Common\BaseTestCase;

class SubSchoolCategoryServiceTest extends BaseTestCase
{
    public function testCreateCategory()
    {
        $category = $this->getCategoryFields();

        $result = $this->getSubSchoolCategoryService()->createCategory($category);

        $this->assertEquals('ceshi', $result['name']);
    }

    public function testGetCategoryTree()
    {
        $category = $this->getCategoryFields();

        $this->getSubSchoolCategoryService()->createCategory($category);
        $result = $this->getSubSchoolCategoryService()->getCategoryTree();

        $this->assertEquals(1, count($result));
    }

    public function testGetCategory()
    {
        $category = $this->getCategoryFields();

        $this->getSubSchoolCategoryService()->createCategory($category);
        $result = $this->getSubSchoolCategoryService()->getCategory(1);

        $this->assertEquals('ceshi', $result['name']);
    }

    public function testGetCategoriesByParentId()
    {
        $category = $this->getCategoryFields();

        $this->getSubSchoolCategoryService()->createCategory($category);
        $result = $this->getSubSchoolCategoryService()->getCategoriesByParentId(0);

        $this->assertEquals(1, count($result));
    }

    public function testUpdateCategory()
    {
        $category = $this->getCategoryFields();
        $this->getSubSchoolCategoryService()->createCategory($category);
        $category['name'] = 'kaifa';

        $result = $this->getSubSchoolCategoryService()->updateCategory(1, $category);

        $this->assertEquals('kaifa', $result['name']);
    }

    public function testDeleteCategory()
    {
        $category = $this->getCategoryFields();
        $this->getSubSchoolCategoryService()->createCategory($category);

        $this->getSubSchoolCategoryService()->deleteCategory(1);
        $result = $this->getSubSchoolCategoryService()->getCategory(1);

        $this->assertEquals(null, $result);
    }

    public function testSearchCategories()
    {
        $category = $this->getCategoryFields();
        $this->getSubSchoolCategoryService()->createCategory($category);
        $conditions = array(
            'name' => 'ceshi',
        );

        $result = $this->getSubSchoolCategoryService()->searchCategories(
            $conditions,
            array('createdTime', 'DESC'),
            0,
            10
        );

        $this->assertEquals(1, count($result));
    }

    public function testFindCategoriesByIds()
    {
        $category = $this->getCategoryFields();
        $this->getSubSchoolCategoryService()->createCategory($category);

        $result = $this->getSubSchoolCategoryService()->findCategoriesByIds(array(1));

        $this->assertEquals(1, count($result));
    }

    public function testSortCategories()
    {
        $category = $this->getCategoryFields();
        $this->getSubSchoolCategoryService()->createCategory($category);

        $this->getSubSchoolCategoryService()->findCategoriesByIds(array(1));
        $result = $this->getSubSchoolCategoryService()->getCategory(1);

        $this->assertEquals(0, $result['seq']);
    }

    public function testGetCategoryByCode()
    {
        $category = $this->getCategoryFields();
        $this->getSubSchoolCategoryService()->createCategory($category);

        $result = $this->getSubSchoolCategoryService()->getCategoryByCode('cate');

        $this->assertEquals('ceshi', $result['name']);
    }

    public function testFindCategoryChildrenIds()
    {
        $category = $this->getCategoryFields();
        $this->getSubSchoolCategoryService()->createCategory($category);

        $result = $this->getSubSchoolCategoryService()->findCategoryChildrenIds(1);

        $this->assertEquals(true, empty($result));
    }

    protected function getCategoryFields()
    {
        $category = array(
            'name' => 'ceshi',
            'code' => 'cate',
        );

        return $category;
    }

    public function testFindCategoryByName()
    {
        $category = $this->getCategoryFields();
        $this->getSubSchoolCategoryService()->createCategory($category);

        $result = $this->getSubSchoolCategoryService()->findCategoryByName('ceshi');

        $this->assertEquals('ceshi', $result['name']);
    }

    protected function getSubSchoolCategoryService()
    {
        return $this->getServiceKernel()->createService('SubSchool:SubSchool.SubSchoolCategoryService');
    }
}