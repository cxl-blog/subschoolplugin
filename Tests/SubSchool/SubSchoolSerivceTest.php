<?php

namespace SubSchool\Service\SubSchool\Tests;

use Topxia\Service\Common\BaseTestCase;
use Topxia\Common\ArrayToolkit;

class SubSchoolServiceTest extends BaseTestCase
{
    public function testCreateAuditRecord()
    {
        $subSchool = array(
            'name' => '测试',
            'address' => 'ss',
            'contact' => 12231,
            'phone' => 13333333333,
            'email' => '1@qq.com',
            'site' => 'ceshi',
            'status' => 'auditing',
            'createdTime' => 1478508987,
            'subSchoolCategoryId' => 1,
            'schoolId' => 1,
            'result' => 'fail',
            'reason' => 'aaaaa',
            'applicantId' => '0'
        );

        $school = $this->getSubSchoolService()->createAuditRecord($subSchool);

        $this->assertEquals('测试', $school['name']);
    }

    public function testGetAuditRecord()
    {
        $subSchool = array(
            'name' => '测试',
            'address' => 'ss',
            'contact' => 12231,
            'phone' => 13333333333,
            'email' => '1@qq.com',
            'site' => 'ceshi',
            'status' => 'auditing',
            'createdTime' => 1478508987,
            'subSchoolCategoryId' => 1,
            'schoolId' => 1,
            'result' => 'fail',
            'reason' => 'aaaaa',
            'applicantId' => '0'
        );

        $this->getSubSchoolService()->createAuditRecord($subSchool);
        $school = $this->getSubSchoolService()->getAuditRecord(1);

        $this->assertEquals('测试', $school['name']);
    }

    public function testSearchAuditRecordCount()
    {
        $subSchool = array(
            'name' => '测试',
            'address' => 'ss',
            'contact' => 12231,
            'phone' => 13333333333,
            'email' => '1@qq.com',
            'site' => 'ceshi',
            'status' => 'auditing',
            'createdTime' => 1478508987,
            'subSchoolCategoryId' => 1,
            'schoolId' => 1,
            'result' => 'fail',
            'reason' => 'aaaaa',
            'applicantId' => '0'
        );
        $conditions = array(
            'schoolId' => 1
        );

        $this->getSubSchoolService()->createAuditRecord($subSchool);
        $count = $this->getSubSchoolService()->searchAuditRecordCount($conditions);

        $this->assertEquals(1, $count);
    }

    public function testSearchAuditRecords()
    {
        $subSchool = array(
            'name' => '测试',
            'address' => 'ss',
            'contact' => 12231,
            'phone' => 13333333333,
            'email' => '1@qq.com',
            'site' => 'ceshi',
            'status' => 'auditing',
            'createdTime' => 1478508987,
            'subSchoolCategoryId' => 1,
            'schoolId' => 1,
            'result' => 'fail',
            'reason' => 'aaaaa',
            'applicantId' => '0'
        );
        $conditions = array(
            'schoolId' => 1
        );
        $this->getSubSchoolService()->createAuditRecord($subSchool);
        $subSchools = $this->getSubSchoolService()->searchAuditRecords(
            $conditions,
            array('createdTime', 'DESC'),
            0,
            10
        );

        $this->assertEquals(1, count($subSchools));
    }

    public function testCreateSchoolUser()
    {
        $user = array(
            'userId' => 1,
            'schoolId' => 1
        );

        $result = $this->getSubSchoolService()->createSchoolUser($user);

        $this->assertEquals(1, $result['userId']);
        $this->assertEquals(1, $result['schoolId']);
    }

    public function testIsUserJoined()
    {
        $user = array(
            'userId' => 1,
            'schoolId' => 1
        );

        $this->getSubSchoolService()->createSchoolUser($user);

        $result = $this->getSubSchoolService()->isUserJoined(1, 1);

        $this->assertEquals(1, $result);

        $result = $this->getSubSchoolService()->isUserJoined(1, 2);

        $this->assertEquals(0, $result);
    }

    public function testFindSchoolUsersByUserId()
    {
        $user = array(
            'userId' => 1,
            'schoolId' => 1
        );
        $user2 = array(
            'userId' => 1,
            'schoolId' => 2
        );

        $this->getSubSchoolService()->createSchoolUser($user);
        $this->getSubSchoolService()->createSchoolUser($user2);

        $result = $this->getSubSchoolService()->findSchoolUsersByUserId(1);
        $users = ArrayToolkit::column($result, 'userId');

        $this->assertEquals(2, count($users));
    }

    public function testFindUserIdsBySchoolId()
    {
        $user = array(
            'userId' => 1,
            'schoolId' => 1
        );
        $user2 = array(
            'userId' => 1,
            'schoolId' => 2
        );

        $this->getSubSchoolService()->createSchoolUser($user);
        $this->getSubSchoolService()->createSchoolUser($user2);

        $result = $this->getSubSchoolService()->findUserIdsBySchoolId(1);

        $this->assertEquals(1, count($result));
    }

    public function testDeleteSubSchoolUserByUserId()
    {
        $user = array(
            'userId' => 1,
            'schoolId' => 1
        );
        $user2 = array(
            'userId' => 1,
            'schoolId' => 2
        );

        $this->getSubSchoolService()->createSchoolUser($user);
        $this->getSubSchoolService()->createSchoolUser($user2);

        $this->getSubSchoolService()->deleteSubSchoolUserByUserId(1);
        $result = $this->getSubSchoolService()->findSchoolUsersByUserId(1);

        $this->assertEquals(0, count($result));
    }

    public function testCreateSubSchool()
    {
        $subSchool = $this->getSubSchoolFields();

        $result = $this->getSubSchoolService()->createSubSchool($subSchool);

        $this->assertEquals('ceshi', $result['name']);
    }

    public function testEditSubSchool()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);
        $subSchool['name'] = 'kaifa';
        $result = $this->getSubSchoolService()->editSubSchool(1, $subSchool);

        $this->assertEquals('kaifa', $result['name']);
    }

    public function testGetSubSchool()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);

        $result = $this->getSubSchoolService()->getSubSchool(1);

        $this->assertEquals('ceshi', $result['name']);
    }

    public function testOpenSubSchool()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);
        $result = $this->getSubSchoolService()->openSubSchool(1);

        $this->assertEquals('normal', $result['status']);
    }

    public function testCloseSubSchool()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);
        $result = $this->getSubSchoolService()->closeSubSchool(1);

        $this->assertEquals('disable', $result['status']);
    }

    public function testSearchSubSchoolsCount()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);
        $conditions = array(
            'name' => 'ceshi',
        );

        $result = $this->getSubSchoolService()->searchSubSchoolsCount($conditions);

        $this->assertEquals(1, $result);
    }

    public function testSearchSubSchools()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);
        $conditions = array(
            'name' => 'ceshi',
        );

        $result = $this->getSubSchoolService()->searchSubSchools(
            $conditions,
            array('createdTime', 'DESC'),
            0,
            10
        );

        $this->assertEquals(1, count($result));
    }

    public function testGetOpenSubSchools()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);
        $this->getSubSchoolService()->openSubSchool(1);
        $result = $this->getSubSchoolService()->getOpenSubSchools();

        $this->assertEquals(1, count($result));
    }

    public function testIsOrgIdAvailable()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);
        $result = $this->getSubSchoolService()->IsOrgIdAvailable(2);

        $this->assertEquals(false, $result);
    }

    public function testGetSubSchoolBySchoolSite()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);
        $result = $this->getSubSchoolService()->getSubSchoolBySchoolSite('ceshi');

        $this->assertEquals('ceshi', $result['name']);
    }

    public function testIsSubSchoolOpen()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);
        $result = $this->getSubSchoolService()->isSubSchoolOpen(1);

        $this->assertEquals(false, $result);
    }

    public function testFindSubSchoolByOrgId()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);
        $result = $this->getSubSchoolService()->findSubSchoolByOrgId(2);

        $this->assertEquals('ceshi', $result['name']);
    }

    public function testFindSubSchoolsByLikeName()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);

        $result = $this->getSubSchoolService()->findSubSchoolsByLikeName('ce');

        $this->assertEquals(1, count($result));
    }

    public function testIsSiteAvailable()
    {
        $subSchool = $this->getSubSchoolFields();

        $this->getSubSchoolService()->createSubSchool($subSchool);

        $result = $this->getSubSchoolService()->isSiteAvailable('ceshi');

        $this->assertEquals(false, empty($result));
    }

    public function testGetSubSchoolByName()
    {
        $subSchool = $this->getSubSchoolFields();
        $this->getSubSchoolService()->createSubSchool($subSchool);

        $result = $this->getSubSchoolService()->getSubSchoolByName('ceshi');

        $this->assertEquals('ceshi', $result['name']);
    }

    public function testGetSubSchoolByApplicantIdAndStatus()
    {
        $subSchool = $this->getSubSchoolFields();
        $this->getSubSchoolService()->createSubSchool($subSchool);

        $result = $this->getSubSchoolService()->getSubSchoolByApplicantIdAndStatus(1, 'auditing');

        $this->assertEquals('ceshi', $result['name']);
    }

    public function testUpdateSubSchool()
    {
        $subSchool = $this->getSubSchoolFields();
        $this->getSubSchoolService()->createSubSchool($subSchool);
        $subSchool['name'] = 'kaifa';

        $result = $this->getSubSchoolService()->updateSubSchool(1, $subSchool);

        $this->assertEquals('kaifa', $result['name']);
    }

    public function testDeleteSubSchool()
    {
        $subSchool = $this->getSubSchoolFields();
        $this->getSubSchoolService()->createSubSchool($subSchool);

        $this->getSubSchoolService()->deleteSubSchool(1);
        $result = $this->getSubSchoolService()->getSubSchool(1);

        $this->assertEquals(null, $result);
    }

    protected function getSubSchoolFields()
    {
        $subSchool = array(
            'name' => 'ceshi',
            'address' => 'aaa',
            'contact' => 'aaa',
            'status' => 'auditing',
            'adminIds' => '1',
            'phone' => '13333333333',
            'email' => '1@qq.com',
            'site' => 'ceshi',
            'logo' => 'file//',
            'cover' => 'file//',
            'subSchoolCategoryId' => '1',
            'branchPrincipal' => 'ceshizhe',
            'principalPhone' => 'ceshizhe',
            'province' => 'zhejiang',
            'proportion' => 10,
            'businessLicense' => 'file//',
            'applicantId' => 1,
            'accountName' => 'gongshang',
            'accountNumber' => '3333',
            'bankName' => 'gongshang',
            'orgCode' => '1.2',
            'orgId' => '2',   
        );

        return $subSchool;      
    }

    public function testFindSubSchoolBySubSchoolCategoryId()
    {
        $subSchool = $this->getSubSchoolFields();
        $this->getSubSchoolService()->createSubSchool($subSchool);
        $result = $this->getSubSchoolService()->findSubSchoolBySubSchoolCategoryId(2);
        $this->assertEquals(null,$result);
    }

    protected function getSubSchoolService()
    {
        return $this->getServiceKernel()->createService('SubSchool:SubSchool.SubSchoolService');
    }
}